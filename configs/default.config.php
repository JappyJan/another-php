<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 14.04.18
 * Time: 21:32
 */

use PHPMailer\PHPMailer\SMTP;

return [
	'version' => '2.0',

	// Base URL of GUI (NOT API)
    'baseUrl' => 'https://another.de',

    'DatabaseManager' => [
        'key' => 'super_secret',
    ],
	
	'settings' => [
		'displayErrorDetails' => false,
		'addContentLengthHeader' => false
	],
	
	'db' => [
		'host' => 'localhost',
		'user' => 'test',
		'pass' => 'test',
		'db' => 'test'
	],

    'logger' => [
        'logFile' => __DIR__ . 'logs/app.log'
    ],
	
	'mail' => [
	    //Debug Output Level
		'SMTPDebug' => SMTP::DEBUG_OFF,
        //use SMTP?
        'isSMTP' => false,
        //Use Authentication? (Username & Password)
        'SMTPAuth' => true,
        //SMTP Port
        'Port' => 88,
        //SMTP Hostname (if multiple Hosts, separate by Semicolon)
		'Host' => 'hostname',
        //SMTP Username
		'Username' => 'username/mail',
        //SMTP Password
		'Password' => 'password',
        //Encryption
        'SMTPSecure' => 'tls',
        //Options to pass to SMTP Server
		'SMTPOptions' => [],

		'From' => [
			'E-Mail' => 'another@php.de',
			'Alias' => 'AnotherPHP'
		],

		'ReplyTo' => [
			'E-Mail' => 'support@php.de',
			'Alias' => 'PHP-Support'
		]
	]
];