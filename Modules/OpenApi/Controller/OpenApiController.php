<?php
/**
 * Created by JanJaap WebSolutions
 *
 * Jan Jaap
 *  https://janaap.de
 *  mail@janjaap.de
 *
 * Date: 29.11.2018
 * Time: 09:59
 */

namespace Modules\OpenApi\Controller;


use Framework\Classes\Controller;
use Framework\Classes\Route;
use Modules\OpenApi\Classes\OpenApi;
use Modules\OpenApi\Classes\RouteParameter;

class OpenApiController extends Controller
{
    /**
     * Open-Api Specification Document (JSON)
     * @throws \ReflectionException
     */
    public function getDefinition()
    {

        $definition = new OpenApi(
            'API',
            '1.0'
        );

        $this->processRoutes($definition);

        echo json_encode($definition);
        die();
    }

    /**
     * @param OpenApi $definition
     * @throws \ReflectionException
     */
    private function processRoutes(OpenApi &$definition)
    {
        /** @var Route[] $routes */
        $routes = $this->config['routing'];
        foreach ($routes as $route) {
            $class = new \ReflectionClass($route->class);
            $method = $class->getMethod($route->function);

            $controllerName = str_replace(
                'Controller',
                '',
                $class->getShortName()
            );
            $methodName = ucfirst($method->getName());

            $tags = [
                $controllerName
            ];

            $summary = '';
            $description = '';
            $this->processDocBlock(
                $method->getDocComment(),
                $summary,
                $description
            );

            $parameters = [];
            $this->processMethodParameters(
                $parameters,
                $method->getParameters()
            );

            $allowedRoles = $route->allowedTypes;

            $definition->addPath(
                $route->route,
                strtolower($route->method),
                $controllerName . $methodName,
                $summary,
                $description,
                $tags,
                $parameters,
                $allowedRoles
            );
        }
    }

    private function processDocBlock(
        string $comment,
        string &$summary = '',
        string &$description = ''
    )
    {
        //define the regular expression pattern to use for string matching
        $pattern = "#([a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)#";

        //perform the regular expression on the string provided
        preg_match_all($pattern, $comment, $lines, PREG_PATTERN_ORDER);

        if (\count($lines) === 0) {
            $lines[0] = [];
        }

        /** @var string $line */
        foreach ($lines[0] as $line) {
            if (strpos($line, 'return') === 0) {
                continue;
            }
            if (strpos($line, 'throws') === 0) {
                continue;
            }

            if (strpos($line, 'Tag ') === 0) {
                $tags[] = str_replace('Tag ', '', $line);
                continue;
            }

            if ($summary === '') {
                $summary = $line;
                continue;
            }


            if ($description !== '') {
                $description .= "\r\n";
            }
            $description .= $line;
        }
    }

    /**
     * @param RouteParameter[] $parameters
     * @param \ReflectionParameter[] $arguments
     */
    private function processMethodParameters(
        array &$parameters,
        array $arguments
    )
    {
        foreach ($arguments as $argument) {
            $name = $argument->getName();
            $isRouuteParam = strpos($name, 'route_') === 0;
            $name = str_replace('route_', '', $name);
            $name = lcfirst($name);

            $type = 'string';
            $format = '';

            switch ($argument->getType()) {
                case 'int':
                    $type = 'integer';
                    break;
                case 'float':
                    $type = 'number';
                    $format = 'float';
                    break;
                case 'DateTime':
                    $type = 'string';
                    $format = 'date-time';
                    break;
                case 'Carbon\Carbon':
                    $type = 'string';
                    $format = 'date-time';
                    break;
                case 'bool':
                    $type = 'boolean';
                    break;
            }

            if (
                $name === 'password' &&
                $type === 'string'
            ) {
                $format = 'password';
            }

            $parameters[] = new RouteParameter(
                $name,
                $isRouuteParam ? 'path' : 'formData',
                $argument->isOptional() === false,
                $type,
                $format
            );
        }
    }
}