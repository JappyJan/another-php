<?php
/**
 * Created by JanJaap WebSolutions
 *
 * Jan Jaap
 *  https://janaap.de
 *  mail@janjaap.de
 *
 * Date: 27.11.2018
 * Time: 17:14
 */

namespace Modules\OpenApi\Classes;


class OpenApi
{
    public $openapi = '3.0.0';
    public $info = [
        'title' => '',
        'version' => '1.0.0'
    ];
    public $paths = [];

    public $servers = [
        [
            'url' => 'http://api.another.de'
        ],
    ];

    public $produces = [
        'application/json'
    ];

    public $components = [
        'securitySchemes' => [
            'ApiKeyAuth' => [
                'type' => 'apiKey',
                'name' => 'token',
                'in' => 'header'
            ]
        ]
    ];

    public function __construct(
        string $apiTitle,
        string $apiVersion
    )
    {
        $this->info['title'] = $apiTitle;
        $this->info['version'] = $apiVersion;

        return $this;
    }

    public function addPath(
        string $route,
        string $method,
        string $operationId,
        string $summary,
        string $description,
        array $tags,
        array $parameters,
        array $allowedRoles
    )
    {
        $this->paths[$route][$method] = [
            'operationId' => $operationId,
            'tags' => $tags,
            'summary' => $summary,
            'description' => $description,
            'responses' => [],
            'parameters' => $parameters,
            'security' => [
                'ApiKeyAuth' => $allowedRoles
            ]
        ];
    }
}

class Parameter
{
    public $name = '';
    public $in = '';
    public $description = '';
    public $required = false;
    public $schema = [
        'type' => '',
        'format' => ''
    ];

    public function __construct(
        string $name,
        string $in,
        string $description,
        bool $required,
        string $schemaType,
        string $schemaFormat = ''
    )
    {
        $this->name = $name;
        $this->in = $in;
        $this->description = $description;
        $this->required = $required;
        $this->schema['type'] = $schemaType;
        $this->schema['format'] = $schemaFormat;

        return $this;
    }
}