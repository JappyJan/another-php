<?php
/**
 * Created by JanJaap WebSolutions
 *
 * Jan Jaap
 *  https://janaap.de
 *  mail@janjaap.de
 *
 * Date: 29.11.2018
 * Time: 23:39
 */

namespace Modules\OpenApi\Classes;


class RouteParameter
{
    public $name = '';
    public $in = '';
    public $required = false;
    public $schema = [
        'type' => 'string',
        'format' => ''
    ];

    public function __construct(
        string $name,
        string $in,
        bool $required,
        string $type,
        string $format = ''
    )
    {
        $this->name = $name;
        $this->in = $in;
        $this->required = $required;
        $this->schema['type'] = $type;
        $this->schema['format'] = $format;
    }
}