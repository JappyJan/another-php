<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 22.04.18
 * Time: 11:29
 */

return [
	'databaseTables' => [
		\Modules\Code\Classes\Code::class
	]
];