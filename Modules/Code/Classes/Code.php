<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 19.04.18
 * Time: 08:59
 */

namespace Modules\Code\Classes;

use Carbon\Carbon;
use Framework\Classes\Database\Table;
use Framework\Classes\Database\TableColumn;
use Framework\Classes\Database\DbObject;

/**
 * Class Code
 * @package Modules\Code\Classes
 */
class Code implements DbObject
{
    /** @var integer */
    private $id;
    /** @var string */
    private $code;
    /** @var string */
    private $type;
    /** @var Carbon|null */
    private $validUntil;
    /** @var string */
    private $identification;
    /** @var string */
    private $payload = '';

    /**
     * @param array $dbObject
     * @return Code
     */
    public static function getFromDbObject(array $dbObject): Code
    {
        $code = new self();

        /** @noinspection PhpUndefinedFieldInspection */
        $code
            ->setId($dbObject['id'])
            ->setCode($dbObject['code'])
            ->setType($dbObject['type'])
            ->setValidUntil(new Carbon($dbObject['validUntil']))
            ->setIdentification($dbObject['identification'])
            ->setPayload($dbObject['payload']);

        return $code;
    }

    /**
     * @param bool $withId
     * @return array
     */
    public function toDbObject(bool $withId = false): array
    {
        $object = [
            'code' => $this->getCode(),
            'type' => $this->getType(),
            'identification' => $this->getIdentification(),
            'validUntil' => null,
            'payload' => $this->getPayload()
        ];

        if ($this->getValidUntil() !== null) {
            $object['validUntil'] = $this->getValidUntil()->toDateTimeString();
        }

        if ($withId) {
            $object['id'] = $this->getId();
        }

        return $object;
    }

    /** @return \Framework\Classes\Database\Table[]
     * @throws \Exception
     */
    public function getDatabaseTables(): array
    {
        $code_table = new Table('code');
        $code_table
            ->addColumns(
                [
                    new TableColumn('id', TableColumn::TYPE_INT, 11, false),
                    new TableColumn('created', TableColumn::TYPE_TIMESTAMP, 0, false, 'CURRENT_TIMESTAMP'),
                    new TableColumn('code', TableColumn::TYPE_VARCHAR, 512, false),
                    new TableColumn('type', TableColumn::TYPE_VARCHAR, 50, false),
                    new TableColumn('validUntil', TableColumn::TYPE_DATETIME, 0, true),
                    new TableColumn('identification', TableColumn::TYPE_VARCHAR, 512, true),
                    new TableColumn('payload', TableColumn::TYPE_VARCHAR, 512, false),
                ]
            )
            ->setPrimaryKey('id', true);

        return [
            $code_table
        ];
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param integer $id
     *
     * @return Code
     */
    public function setId($id): Code
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return Code
     */
    public function setCode($code): Code
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Code
     */
    public function setType($type): Code
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param Carbon $validUntil
     *
     * @return Code
     */
    public function setValidUntil(Carbon $validUntil): Code
    {
        $this->validUntil = $validUntil;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * @param string $identification
     * @return Code
     */
    public function setIdentification(string $identification): Code
    {
        $this->identification = $identification;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayload(): string
    {
        return $this->payload;
    }

    /**
     * @param string $payload
     * @return Code
     */
    public function setPayload(string $payload): Code
    {
        $this->payload = $payload;
        return $this;
    }
}