<?php /** @noinspection NullPointerExceptionInspection */

/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.04.18
 * Time: 00:05
 */

namespace Modules\Code\Models;

use Carbon\Carbon;
use Modules\Code\Classes\Code;
use Modules\DatabaseManager\Classes\SQLException;

/**
 * Class codeModel
 * @package Modules\Code\Models
 */
class CodeModel extends \Framework\Classes\AbstractModel
{
    /**
     * @param string $type
     * @param string $identification
     * @return array
     */
    public function getByTypeAndIdentification(string $type, string $identification): array
    {
        /** @noinspection NullPointerExceptionInspection */
        $db_codes = $this->db
            ->table('code')
            ->where('type', $type)
            ->where('identification', $identification)
            ->fetchAll();

        $codes = [];
        foreach ($db_codes as $db_code) {
            $codes[] = Code::getFromDbObject($db_code->getData());
        }

        return $codes;
    }

    /**
     * @param int $id
     * return Code
     * @return Code
     */
    public function getById(int $id): Code
    {
        $db_code = $this->db
            ->table('code')
            ->where('id', $id)
            ->fetch();

        return Code::getFromDbObject($db_code->getData());
    }

    /**
     * @param Code $code
     * @return Code
     * @throws SQLException
     */
    public function insert(Code $code): Code
    {
        $object = $code->toDbObject();

        $query = $this->db
            ->table('code')
            ->insert(
                [$object],
                'prepared'
            );

        if ($query->errorCode() !== '00000') {
            throw new SQLException('Insert failed!');
        }

        $code->setId($this->db->lastInsertId());
        return $code;
    }

    /**
     * @param Code $code
     * @return Code
     * @throws SQLException
     */
    public function update(Code $code): Code
    {
        $query = $this->db
            ->table('code')
            ->where('id', $code->getId())
            ->update($code->toDbObject());

        if ($query->errorCode() !== '00000') {
            throw new SQLException('Update failed!');
        }
        return $code;
    }

    /**
     * @param string $type
     * @param string $identification
     * @param Carbon|null $validUntil
     * @return null
     * @throws SQLException
     */
    public function invalidateCodes(string $type, string $identification, Carbon $validUntil = null)
    {
        $query = $this->db
            ->table('code')
            ->where('type', $type)
            ->where('identification', $identification);

        if ($validUntil !== null) {
            $query->where('validUntil', $validUntil);
        }

        $stmnt = $query->update(['validUntil' => Carbon::now()]);

        if ($stmnt->errorCode() !== '00000') {
            Throw New SQLException('Something went wrong - ' . $stmnt->errorCode());
        }

        return null;
    }
}