<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de
 * Date: 19.04.18
 * Time: 09:39
 */

namespace Modules\MailTemplate\Models;

use Framework\Config;
use Modules\User\Classes\User;

/**
 * Class MailTemplateModel
 * @package Modules\MailTemplate\Models
 */
class MailTemplateModel
{
    /**
     * @param string $name
     * @param array $replacements
     * @return string
     * @throws \Exception
     */
    private static function get(string $name, array $replacements): string
    {
        $template_path = __DIR__ . '/../Templates/' . $name . '.html';
        if (!file_exists($template_path)) {
            throw new \RuntimeException('Mail-Template existiert nicht');
        }

        $template = file_get_contents($template_path);

        $server_url = Config::get('baseUrl');

        $replacements['server-url'] = $server_url;

        $template = self::replacePlaceholders($template, $replacements);

        return $template;
    }

    /**
     * @param $subject
     * @param $replacements
     * @return string
     */
    private static function replacePlaceholders($subject, $replacements): string
    {
        foreach ($replacements as $placeholder => $value) {
            $subject = self::replacePlaceholder($subject, $placeholder, $value);
        }

        return $subject;
    }

    /**
     * @param string $subject
     * @param string $placeholder
     * @param string $value
     * @return string
     */
    private static function replacePlaceholder(string $subject, string $placeholder, string $value): string
    {
        $subject = str_replace('{{' . $placeholder . '}}', $value, $subject);

        return $subject;
    }
}