<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 20.04.18
 * Time: 18:04
 */

namespace Modules\Api\Controller;

use Framework\Classes\AccessDeniedException;
use Framework\Classes\AnotherException;
use Framework\Classes\Controller;
use LessQL\Database;
use Modules\Api\Models\ApiModel;
use PDO;
use Slim\Container;
use Slim\Http\Request;

/**
 * Class apiController
 * @package Modules\Api\Controller
 */
class ApiController extends Controller
{
    /** @var ApiModel */
    private $model;

    /**
     * apiController constructor.
     * @param Request $request
     * @param Container $c
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Request $request, Container $c)
    {
        parent::__construct($request, $c);

        $this->model = new ApiModel($c);
    }

    /**
     * Displays the current ApiVersion
     * @return array
     */
    public function index(): array
    {
        return ['version' => $this->model->getApplicationVersion()];
    }
}