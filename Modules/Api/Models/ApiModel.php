<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 22.02.18
 * Time: 19:33
 */

namespace Modules\Api\Models;

use Framework\Classes\AbstractModel;

/**
 * Class apiModel
 * @package Modules\Api\Models
 */
class ApiModel extends AbstractModel
{
    /**
     * @return mixed
     */
    public function getApplicationVersion()
    {
        return $this->config['version'];
    }
}