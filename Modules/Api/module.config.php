<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 22.04.18
 * Time: 10:56
 */

use Framework\Classes\Route;
use Modules\Api\Controller\ApiController;

return [
    'routing' => [
        new Route(
            Route::HTTP_GET,
            '/',
            ApiController::class,
            'index'
        ),
    ],
];