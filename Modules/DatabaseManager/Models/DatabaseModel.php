<?php /** @noinspection SqlResolve */

/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 15.05.18
 * Time: 01:51
 */

namespace Modules\DatabaseManager\Models;

use Framework\Classes\AbstractModel;
use Framework\Classes\AnotherException;
use Framework\Classes\Database\Table;
use Framework\Classes\Database\TableColumn;
use Framework\Config;
use Modules\DatabaseManager\Classes\SQLException;
use Modules\DatabaseManager\Classes\TableNotFoundException;
use Slim\Container;

/**
 * Class DatabaseModel
 * @package Modules\DatabaseManager\Models
 */
class DatabaseModel extends AbstractModel
{
    /** @var Table[] */
    private $databaseTables;

    /** @var Table[] */
    private $modelTables;

    /** @var string */
    private $databaseName;

    /**
     * DatabaseModel constructor.
     * @param Container $c
     * @throws \Interop\Container\Exception\ContainerException
     * @throws \Exception
     */
    public function __construct(Container $c)
    {
        parent::__construct($c);

        $this->databaseName = Config::get('db')['db'];

        $this->databaseTables = $this->getDatabaseDefinition();

        $this->modelTables = $this->getModelTableDefinitions();
    }

    /**
     * @return Table[];
     * @throws \Exception
     */
    private function getModelTableDefinitions(): array
    {
        /** @var string[] $models */
        $models = Config::get('databaseTables');

        $model_definitions = [];
        foreach ($models as $model) {
            /** @var \Framework\Classes\Database\DbObject $model_instance */
            $model_instance = new $model();
            $model_tables = $model_instance->getDatabaseTables();
            foreach ($model_tables as $model_table) {
                $model_definitions[] = $model_table;
            }
        }

        return $model_definitions;
    }

    /**
     * @return Table[];
     * @throws \Exception
     */
    private function getDatabaseDefinition(): array
    {
        $definitions = $this->fetchDatabaseDefinition();

        return $this->convertDatabaseTables($definitions);
    }

    /**
     * @return array
     */
    private function fetchDatabaseDefinition(): array
    {
        $tables = $this->getDatabaseTableNames();

        $table_definitions = [];
        foreach ($tables as $table) {
            $stmnt = $this->db->query('DESCRIBE ' . $table);
            $stmnt->execute();
            $table_definitions[$table] = $stmnt->fetchAll();

            $table_definitions[$table]['uniques'] = $this->fetchDatabaseTableUniqueKeys($table);
        }

        return $table_definitions;
    }

    /**
     * @param string $tableName
     * @return array
     */
    private function fetchDatabaseTableUniqueKeys(string $tableName): array
    {
        $unique_keys = [];

        $stmnt = $this->db->query(
            "
                    SELECT DISTINCT 
                      constraint_name 
                    FROM 
                      information_schema.TABLE_CONSTRAINTS
                    WHERE 
                      constraint_schema = '" . $this->databaseName . "' AND
                      table_name = '" . $tableName . "' AND 
                      constraint_type = 'UNIQUE'
                      "
        );
        $stmnt->execute();
        $db_key_names = $stmnt->fetchAll();

        $key_names = [];
        foreach ($db_key_names as $db_key_name) {
            $key_names[] = $db_key_name['constraint_name'];
        }

        foreach ($key_names as $key_name) {
            $stmnt = $this->db->query(
                "
                        SELECT 
                          column_name
                        FROM
                          information_schema.key_column_usage 
                        WHERE 
                          constraint_schema = '" . $this->databaseName . "' AND 
                          table_name = '" . $tableName . "' AND
                          constraint_name = '" . $key_name . "'
                          "
            );
            $stmnt->execute();

            $db_columns = $stmnt->fetchAll();

            $column_string = '';
            foreach ($db_columns as $db_column) {
                $column_string = $column_string . ',' . $db_column['column_name'];
            }

            if (\strlen($column_string) > 0) {
                $column_string = substr($column_string, 1);
            }

            $unique_keys[$key_name] = $column_string;
        }

        return $unique_keys;
    }

    /**
     * @return string[]
     */
    private function getDatabaseTableNames(): array
    {
        $stmnt = $this->db->query('SHOW TABLES');
        $stmnt->execute();
        $results = $stmnt->fetchAll();

        $table_names = [];
        foreach ($results as $result) {
            foreach ($result as $table) {
                $table_names[] = $table;
            }
        }

        return $table_names;
    }

    /**
     * @param array $databaseTables
     * @return Table[]
     * @throws \Exception
     */
    public function convertDatabaseTables(array $databaseTables): array
    {
        /** @var Table[] $tables */
        $tables = [];

        foreach ($databaseTables as $table_name => $table_definition) {
            $table = new Table($table_name);

            foreach ($table_definition as $key => $table_column) {
                if ($key === 'uniques') {
                    $table = $this->addUniques($table, $table_column);
                    continue;
                }

                $table = $this->addColumn($table, $table_column);
            }

            $tables[] = $table;
        }

        return $tables;
    }

    /**
     * @param Table $table
     * @param array $uniques
     * @return Table
     */
    private function addUniques(Table $table, array $uniques): Table
    {
        foreach ($uniques as $key_name => $column_string) {
            $table->addUniqueColumn($key_name, $column_string);
        }

        return $table;
    }

    /**
     * @param Table $table
     * @param array $table_column
     * @return Table
     * @throws \Exception
     */
    private function addColumn(Table $table, array $table_column): Table
    {
        $name = $table_column['Field'];
        $nullable = $table_column['Null'] !== 'NO';
        $key = $table_column['Key'];
        $column_default = $table_column['Default'];

        $default = $column_default;
        if ($column_default === null) {
            $default = '';
        }

        $extra = $table_column['Extra'];

        $primary = $key === 'PRI';
        $auto_increment = false;
        if ($extra === 'auto_increment') {
            $auto_increment = true;
        }

        $column_type = $table_column['Type'];
        $type = strtoupper($column_type);
        $length = 0;
        if (strpos($column_type, '(')) {
            $length = substr($column_type, strpos($column_type, '(') + 1);
            $length = substr($length, 0, -1);

            $type = substr($column_type, 0, \strlen($column_type) - \strlen($length . '()'));
            $type = strtoupper($type);
            $length = (int)$length;

            if ($type === 'VAR') {
                $type = TableColumn::TYPE_VARCHAR;
            }
        }

        $table->addColumn(
            new TableColumn(
                $name,
                $type,
                $length,
                $nullable,
                $default
            )
        );

        if ($primary) {
            $table->setPrimaryKey($name, $auto_increment);
        }

        return $table;
    }

    /**
     * @return Table[];
     */
    public function getNewTables(): array
    {
        $new_tables = [];
        foreach ($this->modelTables as $model_table) {
            $matches = [];
            foreach ($this->databaseTables as $database_table) {
                if ($database_table->getName() === $model_table->getName()) {
                    $matches[] = $model_table;
                }
            }

            if (\count($matches) === 0) {
                $new_tables[] = $model_table;
            }
        }

        return $new_tables;
    }

    /**
     * @return Table[]
     */
    public function getDroppedTables(): array
    {
        $dropped_tables = [];

        /** @var Table $database_table */
        foreach ($this->databaseTables as $database_table) {
            $dropped = true;
            foreach ($this->modelTables as $model_table) {
                if ($model_table->getName() === $database_table->getName()) {
                    $dropped = false;
                    break;
                }
            }

            if ($dropped) {
                $dropped_tables[] = $database_table;
            }
        }

        return $dropped_tables;
    }

    /**
     * @return Table[]
     * @throws AnotherException
     */
    public function getChangedTables(): array
    {
        $changed_tables = [];
        $dropped_tables = $this->getDroppedTables();

        /** @var Table $database_table */
        foreach ($this->databaseTables as $database_table) {
            //skip dropped tables
            if (
                \count(
                    array_filter(
                        $dropped_tables,
                        Function (Table $droppedTable) use ($database_table) {
                            return $database_table->getName() === $droppedTable->getName();
                        }
                    )
                ) > 0
            ) {
                continue;
            }

            $model_table = array_filter(
                $this->modelTables,
                Function (Table $table) use ($database_table) {
                    return $table->getName() === $database_table->getName();
                }
            );

            if (!\is_array($model_table) || \count($model_table) !== 1) {
                throw new AnotherException('Table ' . $database_table->getName() . " is dropped in Models but not in Database, please run 'DatabaseManager --dropTables' first!\r\n");
            }

            /** @noinspection LoopWhichDoesNotLoopInspection */
            foreach ($model_table as $table) {
                //cant use first key, because we don't know the first key...
                $model_table = $table;
                break;
            }

            if (
                $model_table->getName() !== $database_table->getName() ||
                $model_table->getPrimaryKeyName() !== $database_table->getPrimaryKeyName() ||
                $model_table->primaryKeyIsAutoIncrement() !== $database_table->primaryKeyIsAutoIncrement()
            ) {
                $changed_tables[] = $model_table;
                continue;
            }

            if ($this->columnsAreDifferent($model_table->getColumns(), $database_table->getColumns())) {
                $changed_tables[] = $model_table;
                continue;
            }

            foreach ($model_table->getUniqueColumns() as $model_unique_column) {
                $identical = false;
                foreach ($database_table->getUniqueColumns() as $db_unique_column) {
                    if ($model_unique_column['keyName'] !== $db_unique_column['keyName']) {
                        continue;
                    }

                    if ($model_unique_column['columnName'] !== $db_unique_column['columnName']) {
                        continue;
                    }

                    $identical = true;
                    break;
                }

                if (!$identical) {
                    $changed_tables[] = $model_table;
                    continue;
                }
            }
        }

        return $changed_tables;
    }

    /**
     * @param \Framework\Classes\Database\TableColumn[] $modelCols
     * @param \Framework\Classes\Database\TableColumn[] $db_cols
     *
     * @return bool
     */
    private function columnsAreDifferent(array $modelCols, array $db_cols): bool
    {
        if (\count($modelCols) !== \count($db_cols)) {
            return true;
        }

        $changed = false;
        foreach ($modelCols as $model_col) {
            $db_col = array_filter(
                $db_cols, Function (TableColumn $col) use ($model_col) {
                return $model_col->getName() === $col->getName();
            }
            );

            if (!\is_array($db_col) || \count($db_col) !== 1) {
                $changed = true;
                break;
            }

            foreach ($db_col as $col) {
                $db_col = $col;
            }

            if (
                $model_col->getName() !== $db_col->getName() ||
                $model_col->getDefault() !== $db_col->getDefault() ||
                $model_col->getLength() !== $db_col->getLength() ||
                $model_col->getType() !== $db_col->getType()
            ) {
                $changed = true;
                break;
            }
        }

        return $changed;
    }

    /**
     * @param string $table_name
     * @throws SQLException
     */
    public function createTable(string $table_name)
    {
        $tables = $this->getTableDefinitions($table_name);
        $model_table = $tables['model_table'];

        $sql = $this->buildCreateString($model_table);

        $result = $this->db->prepare($sql)->execute();

        if (!$result) {
            Throw new SQLException("Could not Create Table '" . $model_table->getName());
        }

        try {
            $this->createUniques($model_table);
        } catch (SQLException $e) {
            $this->dropTable($table_name);
            throw new SQLException('Unique Creation failed!');
        }
    }

    /**
     * @param Table $model_table
     * @return string
     */
    private function buildCreateString(Table $model_table): string
    {
        $table_part = '`' . $this->databaseName . '`.`' . $model_table->getName() . '`';
        $columns = '(';

        foreach ($model_table->getColumns() as $column) {
            $column_string = '`' . $column->getName() . '` ' . $column->getType();
            if ($column->getLength() > 0) {
                $column_string .= '(' . $column->getLength() . ')';
            }
            if (!$column->isNullable()) {
                $column_string .= ' NOT NULL';
            }
            if ($model_table->primaryKeyIsAutoIncrement() && $model_table->getPrimaryKeyName() === $column->getName()) {
                $column_string .= ' AUTO_INCREMENT';
            }
            if (\strlen($column->getDefault()) > 0) {
                $column_string .= ' DEFAULT ' . $column->getDefault();
            }

            $columns .= $column_string . ',';
        }

        if (\strlen($model_table->getPrimaryKeyName()) > 0) {
            $columns .= 'PRIMARY KEY (`' . $model_table->getPrimaryKeyName() . '`),';
        }

        $columns = substr($columns, 0, -1);

        $columns .= ')';

        return 'CREATE TABLE ' . $table_part . $columns;
    }

    /**
     * @param Table $db_table
     * @throws SQLException
     */
    private function deleteUniques(Table $db_table)
    {
        foreach ($db_table->getUniqueColumns() as $unique) {
            $sql = 'DROP INDEX ' . $unique['keyName'] . ' ON ' . $db_table->getName();
            $result = $this->db->prepare($sql)->execute();

            if (!$result) {
                Throw new SQLException("Could not DROP UNIQUE '" . $unique['keyName'] . "' at Table '" . $db_table->getName());
            }
        }
    }

    /**
     * @param Table $model_table
     * @throws SQLException
     */
    private function createUniques(Table $model_table)
    {
        foreach ($model_table->getUniqueColumns() as $unique_column) {
            $sql = 'ALTER TABLE ' . $model_table->getName() . ' ADD CONSTRAINT ' . $unique_column['keyName'] . ' UNIQUE (' . $unique_column['columnName'] . ')';

            $result = $this->db->prepare($sql)->execute();

            if (!$result) {
                Throw new SQLException("Could not add UNIQUE '" . $unique_column['keyName'] . "' to Table '" . $model_table->getName());
            }
        }
    }

    /**
     * @param string $table_name
     * @throws SQLException
     * @throws TableNotFoundException
     */
    public function alterTable(string $table_name)
    {
        $tables = $this->getTableDefinitions($table_name);
        $db_table = $tables['db_table'];
        $model_table = $tables['model_table'];

        if ($db_table === null) {
            Throw new TableNotFoundException($table_name);
        }

        $sql = $this->buildAlterString($model_table, $db_table);

        $result = $this->db->prepare($sql)->execute();

        if (!$result) {
            Throw new SQLException("Could not Update Table '" . $table_name);
        }

        try {
            $this->deleteUniques($db_table);
            $this->createUniques($model_table);
        } catch (SQLException $exception) {
            throw new SQLException('altering of Uniques failed');
        }
    }

    /**
     * @param Table $model_table
     * @param Table $db_table
     * @return string
     */
    private function buildAlterString(Table $model_table, Table $db_table): string
    {
        $changes = '';
        foreach ($model_table->getColumns() as $key => $model_column) {
            /** @var TableColumn[] $db_column */
            $db_column = array_filter(
                $db_table->getColumns(),
                Function (TableColumn $col) use ($model_column) {
                    return $model_column->getName() === $col->getName();
                }
            );

            $change_type = 'CHANGE';
            if (\count($db_column) === 0) {
                //drop column;
                $change_type = 'ADD';
            }

            /** @noinspection LoopWhichDoesNotLoopInspection */
            foreach ($db_column as $col) {
                $db_column = $col;
                break;
            }

            $change_string = $change_type . ' ';
            if (!\is_array($db_column)) {
                $change_string .= '`' . $db_column->getName() . '` ';
            }

            $change_string .= '`' . $model_column->getName() . '` ' . $model_column->getType();

            if ($model_column->getLength() > 0) {
                $change_string .= '(' . $model_column->getLength() . ')';
            }
            if (!$model_column->isNullable()) {
                $change_string .= ' NOT NULL';
            }
            if ($model_table->primaryKeyIsAutoIncrement() && $model_table->getPrimaryKeyName() === $model_column->getName()) {
                $change_string .= ' AUTO_INCREMENT';
            }
            if (\strlen($model_column->getDefault()) > 0) {
                $change_string .= ' DEFAULT ' . $model_column->getDefault();
            }

            $changes .= ',' . $change_string;
        }

        foreach ($db_table->getColumns() as $db_column) {
            $model_column = array_filter(
                $model_table->getColumns(), Function (TableColumn $col) use ($db_column) {
                return $col->getName() === $db_column->getName();
            }
            );

            if (!\is_array($model_column) || \count($model_column) === 0) {
                $changes .= ',DROP COLUMN `' . $db_column->getName() . '`';
            }
        }

        $changes = substr($changes, 1);

        $sql = 'use `' . $this->databaseName . '`;';
        /** @noinspection SyntaxError */
        $sql .= 'ALTER TABLE `' . $model_table->getName() . '` ' . $changes;
        return $sql;
    }

    /**
     * @param string $table_name
     * @throws SQLException
     */
    public function dropTable(string $table_name)
    {
        $tables = $this->getTableDefinitions($table_name);
        $db_table = $tables['db_table'];

        $sql = 'DROP TABLE ' . $db_table->getName();
        $result = $this->db->prepare($sql)->execute();

        if (!$result) {
            Throw new SQLException("Could not DROP Table '" . $table_name);
        }
    }

    /**
     * @param string $table_name
     * @return array
     */
    private function getTableDefinitions(string $table_name): array
    {
        $db_table = null;
        $model_table = null;

        /** @var Table $table */
        foreach ($this->modelTables as $table) {
            if ($table->getName() === $table_name) {
                $model_table = $table;
                break;
            }
        }

        foreach ($this->databaseTables as $table) {
            if ($table->getName() === $table_name) {
                $db_table = $table;
                break;
            }
        }

        return [
            'db_table' => $db_table,
            'model_table' => $model_table
        ];
    }
}