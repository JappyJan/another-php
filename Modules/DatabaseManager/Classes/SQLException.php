<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Modules\DatabaseManager\Classes;

use Framework\Classes\AnotherException;

/**
 * Class SQLException
 * @package Modules\DatabaseManager\Classes
 */
class SQLException extends AnotherException
{
    /**
     * SQLException constructor.
     * @param $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}