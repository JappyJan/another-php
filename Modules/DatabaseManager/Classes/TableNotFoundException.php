<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Modules\DatabaseManager\Classes;

use Framework\Classes\AnotherException;

/**
 * Class SQLException
 * @package Modules\DatabaseManager\Classes
 */
class TableNotFoundException extends AnotherException
{
    /**
     * SQLException constructor.
     * @param string $table_name
     */
    public function __construct(string $table_name)
    {
        parent::__construct("Tabelle '" . $table_name . "' nicht gefunden'");
    }
}