<?php /** @noinspection SpellCheckingInspection */

/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 25.04.18
 * Time: 22:35
 */

namespace Modules\DatabaseManager\Middleware;

use Framework\Classes\Middleware\Middleware;
use Framework\Classes\Response\JsonResponse;
use Framework\Config;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AccessControl
 * @package Modules\DatabaseManager\Middleware
 */
class AccessControl implements Middleware
{
    /** @var Container */
    private $container;

    /**
     * GeneralAccessControl constructor.
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        /** @noinspection UnusedConstructorDependenciesInspection */
        $this->container = $c;
    }

    /**
     * @noinspection SpellCheckingInspection
     * @param Request $request
     * @param Response $response
     * @param $nextMiddleware
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response, $nextMiddleware)
    {
        if (!$request->hasHeader('key')) {
            return $response->withJson(new JsonResponse(JsonResponse::CODE_ACCESS_DENIED, 'Access-Key missing'));
        }

        $request_key = $request->getHeader('key')[0];
        $key = Config::get('DatabaseManager')['key'];

        if ($request_key !== $key) {
            return $response->withJson(new JsonResponse(JsonResponse::CODE_ACCESS_DENIED, 'ACCESS DENIED'));
        }

        return $nextMiddleware($request, $response);
    }
}