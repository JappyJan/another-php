<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 20.04.18
 * Time: 18:04
 */

namespace Modules\DatabaseManager\Controller;

use Framework\Classes\Controller;
use Modules\Api\Models\ApiModel;
use Modules\DatabaseManager\Models\DatabaseModel;
use Slim\Container;
use Slim\Http\Request;

/**
 * Class apiController
 * @package Modules\Api\Controller
 */
class DatabaseManagerController extends Controller
{
    /** @var ApiModel */
    private $model;

    /**
     * apiController constructor.
     * @param Request $request
     * @param Container $c
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Request $request, Container $c)
    {
        parent::__construct($request, $c);

        $this->model = new DatabaseModel($c);
    }

    /**
     * Get all Tables that ar not up-to-date in the current DB
     * @return array
     * @throws \Framework\Classes\AnotherException
     */
    public function getAllChanges(): array
    {
        $data = [
            'new' => [],
            'changed' => [],
            'dropped' => [],
        ];

        $new = $this->model->getNewTables();
        $changed = $this->model->getChangedTables();
        $dropped = $this->model->getDroppedTables();

        foreach ($new as $table) {
            $data['new'][] = $table->serialize();
        }
        foreach ($changed as $table) {
            $data['changed'][] = $table->serialize();
        }
        foreach ($dropped as $table) {
            $data['dropped'][] = $table->serialize();
        }

        return $data;
    }

    /**
     * Create a specific Table
     * @PostParam name required
     * @param string $name
     * @return bool
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function createTable(string $name): bool
    {
        $this->model->createTable($name);

        return true;
    }

    /**
     * Update a specific Table
     * @PostParam name required
     * @param string $name
     * @return bool
     * @throws \Modules\DatabaseManager\Classes\SQLException
     * @throws \Modules\DatabaseManager\Classes\TableNotFoundException
     */
    public function alterTable(string $name): bool
    {
        $this->model->alterTable($name);

        return true;
    }

    /**
     * Delete a specific Table
     * @PostParam name required
     * @param string $name
     * @return bool
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function dropTable(string $name): bool
    {
        $this->model->dropTable($name);

        return true;
    }
}