<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 22.04.18
 * Time: 10:56
 */

use Framework\Classes\Route;

return [
    'routing' => [
        new Route(
            Route::HTTP_GET,
            '/DatabaseManager/changes',
            \Modules\DatabaseManager\Controller\DatabaseManagerController::class,
            'getAllChanges',
            [\Modules\DatabaseManager\Middleware\AccessControl::class]
        ),

        new Route(
            Route::HTTP_POST,
            '/DatabaseManager/create',
            \Modules\DatabaseManager\Controller\DatabaseManagerController::class,
            'createTable',
            [\Modules\DatabaseManager\Middleware\AccessControl::class]
        ),

        new Route(
            Route::HTTP_POST,
            '/DatabaseManager/alter',
            \Modules\DatabaseManager\Controller\DatabaseManagerController::class,
            'alterTable',
            [\Modules\DatabaseManager\Middleware\AccessControl::class]
        ),

        new Route(
            Route::HTTP_POST,
            '/DatabaseManager/drop',
            \Modules\DatabaseManager\Controller\DatabaseManagerController::class,
            'dropTable',
            [\Modules\DatabaseManager\Middleware\AccessControl::class]
        )
    ],
];