<?php /** @noinspection NullPointerExceptionInspection */

/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 14.04.18
 * Time: 20:35
 */

namespace Modules\User\Models;

use Carbon\Carbon;
use Framework\Classes\AbstractModel;
use Modules\Code\Classes\Code;
use Modules\DatabaseManager\Classes\SQLException;
use Modules\User\Classes\AccountInactiveException;
use Modules\User\Classes\AccountUnknownException;
use Modules\User\Classes\InvalidActivationCodeException;
use Modules\User\Classes\InvalidApiTokenException;
use Modules\User\Classes\UnknownUserTypeException;
use Modules\User\Classes\User;

/**
 * Class userModel
 * @package Modules\User\Models
 */
class UserModel extends AbstractModel
{
    /**
     * @param User $user
     * @return User
     * @throws SQLException
     */
    public function insert(User $user): User
    {
        $stmnt = $this->db
            ->table('user')
            ->insert(
                [$user->toDbObject(false, true, false, false)],
                'prepared'
            );

        if ($stmnt->errorCode() !== '00000') {
            Throw New SQLException('Something went wrong - ' . $stmnt->errorCode());
        }

        $user->setId($this->db->lastInsertId());

        return $user;
    }

    /**
     * @param User $user
     * @return User
     * @throws SQLException
     */
    public function update(User $user): User
    {
        $stmnt = $this->db
            ->table('user')
            ->where('id', $user->getId())
            ->update($user->toDbObject(false, true, false, false, true));

        if ($stmnt->errorCode() !== '00000') {
            Throw New SQLException('Something went wrong - ' . $stmnt->errorCode());
        }

        return $user;
    }

    /**
     * @param User $user
     * @throws SQLException
     */
    public function hardDelete(User $user)
    {
        $stmnt = $this->db
            ->table('user')
            ->where('id', $user->getId())
            ->delete();

        if ($stmnt->errorCode() !== '00000') {
            Throw New SQLException('Something went wrong - ' . $stmnt->errorCode());
        }
    }

    /**
     * @param string $mail
     * @return bool
     */
    public function mailIsInUse(string $mail): bool
    {
        $user = $this->db
            ->table('user')
            ->where('mail', $mail)
            ->fetch();

        return $user !== null;
    }

    /**
     * @param string $nickname
     * @return bool
     */
    public function nicknameIsInUse(string $nickname): bool
    {
        $user = $this
            ->db
            ->table('user')
            ->where('nickname', $nickname)
            ->fetch();

        return $user !== null;
    }

    /**
     * @param string $nickname
     * @param string $token
     * @return User
     * @throws AccountUnknownException
     * @throws InvalidActivationCodeException
     * @throws UnknownUserTypeException
     */
    public function activate(string $nickname, string $token): User
    {
        $db_user = $this->db
            ->table('user')
            ->where('nickname', $nickname)
            ->fetch();

        if ($db_user === null) {
            throw new AccountUnknownException();
        }

        $user_data = $db_user->getData();

        /** @noinspection CallableParameterUseCaseInTypeContextInspection */
        $token = $this->db
            ->table('code')
            ->where('type', 'activation')
            ->where('code', $token)
            ->where('identification', $user_data['mail'])
            ->fetch();

        if ($token === null) {
            throw new InvalidActivationCodeException();
        }

        $db_user
            ->update([
                'active' => 1
            ]);

        return User::getFromDbObject($db_user->getData());
    }

    /**
     * @param string $mailOrNickname
     * @param string $password
     * @param bool $preventKeyRenewing
     * @return bool|User
     * @throws AccountInactiveException
     * @throws AccountUnknownException
     * @throws SQLException
     * @throws UnknownUserTypeException
     * @throws \Exception
     */
    public function signIn(string $mailOrNickname, string $password, bool $preventKeyRenewing = false)
    {
        $db_user = $this->db
            ->table('user')
            ->where('mail', $mailOrNickname)
            ->fetch();

        if ($db_user === null) {
            $db_user = $this->db
                ->table('user')
                ->where('nickname', $mailOrNickname)
                ->fetch();
        }

        if ($db_user === null) {
            throw new AccountUnknownException();
        }

        $user = User::getFromDbObject($db_user->getData());

        if ($user->isActive() === false) {
            throw new AccountInactiveException();
        }

        if (!password_verify($password, $user->getPasswordHash())) {
            throw new AccountUnknownException();
        }

        if ($preventKeyRenewing) {
            return $user;
        }

        $now = Carbon::now();
        $valid_until = $now->addMinutes(30);

        //delete old tokens
        $stmnt = $this->db
            ->table('code')
            ->where('type = ? AND validUntil < ?', ['api_token', Carbon::now()->toDateTimeString()])
            ->delete();

        if ($stmnt->errorCode() !== '00000') {
            Throw New SQLException('Something went wrong - ' . $stmnt->errorCode());
        }

        $user->setToken(sha1($user->getMail() . $user->getPasswordHash() . random_int(1, 999999999)));


        $stmnt = $this->db
            ->table('code')
            ->insert(
                [
                    [
                        'type' => 'api_token',
                        'code' => $user->getToken(),
                        'validUntil' => $valid_until->toDateTimeString(),
                        'identification' => $user->getMail()
                    ]
                ],
                'prepared'
            );

        if ($stmnt->errorCode() !== '00000') {
            Throw New SQLException('Something went wrong - ' . $stmnt->errorCode());
        }

        return $user;
    }

    /**
     * @param string $token
     * @return bool
     * @throws SQLException
     */
    public function signOut(string $token): bool
    {
        $stmnt = $this->db
            ->table('code')
            ->where('code', $token)
            ->delete();

        if ($stmnt->errorCode() !== '00000') {
            Throw New SQLException('Something went wrong - ' . $stmnt->errorCode());
        }

        return true;
    }

    /**
     * Get a single User
     * @param int $userId
     * @return User
     * @throws AccountUnknownException
     * @throws UnknownUserTypeException
     */
    public function getById(int $userId): User
    {

        $user = $this->db
            ->table('user')
            ->where('id', $userId)
            ->fetch();

        if ($user === null) {
            throw new AccountUnknownException();
        }

        unset($user->password_hash);
        return User::getFromDbObject($user->getData());
    }

    /**
     * Get All Users
     * @return User[]
     * @throws UnknownUserTypeException
     */
    public function getAll(): array
    {
        $db_users = $this->db
            ->table('user')
            ->fetchAll();

        $users = [];
        foreach ($db_users as $db_user) {
            $users[] = User::getFromDbObject($db_user->getData());
        }
        return $users;
    }

    /**
     * @param int $user_id
     * @param string $token
     * @return User
     * @throws AccountUnknownException
     * @throws InvalidApiTokenException
     * @throws UnknownUserTypeException
     */
    public function changeMail(int $user_id, string $token): User
    {
        $db_user = $this->db
            ->table('user')
            ->where('id', $user_id)
            ->fetch();

        if ($db_user === null) {
            throw new AccountUnknownException();
        }

        $now = Carbon::now();

        /** @noinspection CallableParameterUseCaseInTypeContextInspection */
        $token = $this->db
            ->table('code')
            ->where('type', 'change_mail')
            ->where('code', $token)
            ->where('identification', $user_id)
            ->where('validUntil > ?', [$now->toDateTimeString()])
            ->fetch();

        if ($token === null) {
            throw new InvalidApiTokenException();
        }

        $code = Code::getFromDbObject($token->getData());

        $db_user
            ->update([
                'mail' => $code->getPayload()
            ]);

        return User::getFromDbObject($db_user->getData());
    }

    /**
     * @param $nickname
     * @return User
     * @throws AccountUnknownException
     * @throws UnknownUserTypeException
     */
    public function getByNickname($nickname)
    {
        $user = $this->db
            ->table('user')
            ->where('nickname', $nickname)
            ->fetch();

        if ($user === null) {
            throw new AccountUnknownException();
        }

        unset($user->password_hash);
        return User::getFromDbObject($user->getData());
    }

    /**
     * @param $mail
     * @return User
     * @throws AccountUnknownException
     * @throws UnknownUserTypeException
     */
    public function getByMail($mail)
    {
        $user = $this->db
        ->table('user')
        ->where('mail', $mail)
        ->fetch();

        if ($user === null) {
            throw new AccountUnknownException();
        }

        unset($user->password_hash);
        return User::getFromDbObject($user->getData());
    }
}