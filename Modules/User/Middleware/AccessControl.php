<?php /** @noinspection SpellCheckingInspection */

/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 25.04.18
 * Time: 22:35
 */

namespace Modules\User\Middleware;

use Carbon\Carbon;
use Framework\Classes\AccessDeniedException;
use Framework\Classes\Middleware\Middleware;
use Framework\Classes\Response\JsonResponse;
use Framework\Classes\Route;
use Framework\Config;
use Modules\Code\Classes\Code;
use Modules\Code\Models\CodeModel;
use Modules\User\Classes\InvalidApiTokenException;
use Modules\User\Classes\NotSignedInException;
use Modules\User\Classes\User;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class GeneralAccessControl
 * @package Modules\User\Middleware
 */
class AccessControl implements Middleware
{
    /** @var Container */
    private $container;

    /**
     * GeneralAccessControl constructor.
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        $this->container = $c;
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $nextMiddleware
     * @return mixed|Response
     * @throws \Framework\Classes\ModuleConfigNotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function __invoke(Request $request, Response $response, $nextMiddleware)
    {
        $route = $request->getUri()->getPath();

        $allowed_types = $this->getAllowedUserTypesForRoute($route, $request->getMethod());

        // Route kann von jedem besichtigt werden
        if (\count($allowed_types) === 0) {
            if (strpos($route, '/DatabaseManager') !== 0) {
                try {
                    // set current User before continue
                    if ($request->hasHeader('token')) {
                        $this->authorizeByToken($request->getHeader('token')[0]);
                    }
                } catch (\Exception $e) {
                    // Yes... this is ugly... I know...
                }
            }
            return $nextMiddleware($request, $response);
        }

        // kein Token-Header vorhanden
        if (!$request->hasHeader('token')) {
            $e = new NotSignedInException();
            return $response->withJson(
                new JsonResponse(
                    JsonResponse::CODE_USER_NOT_SIGNED_IN,
                    $e->getMessage()
                )
            );
        }

        try {
            $this->authorizeByToken($request->getHeader('token')[0]);
            /** @var User $cur_user */
            $cur_user = $this->container->get('currentUser');

            foreach ($allowed_types as $allowed_type) {
                if (
                    $allowed_type !== '*' &&
                    (
                        $allowed_type !== $cur_user->getType() ||
                        !$cur_user->isActive()
                    )
                ) {
                    continue;
                }

                return $nextMiddleware($request, $response);
            }
        } catch (InvalidApiTokenException $e) {
            return $response->withJson(
                new JsonResponse(
                    JsonResponse::CODE_USER_NOT_SIGNED_IN,
                    $e->getMessage()
                )
            );
        }

        $e = new AccessDeniedException();
        return $response->withJson(
            new JsonResponse(
                JsonResponse::CODE_ACCESS_DENIED,
                $e->getMessage()
            )
        );
    }

    /**
     * @param string $route
     * @param string $method
     * @return array
     * @throws \Framework\Classes\ModuleConfigNotFoundException
     */
    private function getAllowedUserTypesForRoute(string $route, string $method): array
    {
        $route_configs = Config::get('routing');

        $splitted_route = explode('/', $route);
        /** @var Route[] $route_configs */
        foreach ($route_configs as $config) {
            if ($config->method !== $method) {
                continue;
            }

            $splitted_conf = explode('/', $config->route);

            if (\count($splitted_route) !== \count($splitted_conf)) {
                continue;
            }

            $same = true;
            foreach ($splitted_conf as $index => $conf_part) {
                if (strpos($conf_part, '{') === 0) {
                    continue;
                }

                if ($conf_part !== $splitted_route[$index]) {
                    $same = false;
                    break;
                }
            }

            if (!$same) {
                continue;
            }

            return $config->allowedTypes;
        }

        return [];
    }

    /**
     * @param string $apiToken
     * @throws InvalidApiTokenException
     * @throws \Interop\Container\Exception\ContainerException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    private function authorizeByToken(string $apiToken)
    {

        if ($apiToken === '') {
            throw new InvalidApiTokenException();
        }

        $db_token = $this->apiTokenIsValid($apiToken);

        if (!$db_token) {
            throw new InvalidApiTokenException();
        }

        $this->extendApiTokenValidity($db_token);
    }

    /**
     * checks wether the provided API Token ist valid, and that there is an active User that relates to the Token
     * @param string $apiToken
     * @return Code | false
     * @throws \Interop\Container\Exception\ContainerException
     * @throws \Exception
     */
    private function apiTokenIsValid(string $apiToken)
    {
        /** @var $db \LessQL\Database */
        $db = $this->container->get('db');
        $now = Carbon::now();

        /** @noinspection NullPointerExceptionInspection */
        $token = $db
            ->table('code')
            ->where('type', 'api_token')
            ->where('code', $apiToken)
            ->where('validUntil > ?', $now->toDateTimeString())
            ->fetch();

        if ($token === null) {
            return false;
        }

        try {
            $token = Code::getFromDbObject($token->getData());

            /** @noinspection NullPointerExceptionInspection */
            $user = $db
                ->table('user')
                ->where('mail', $token->getIdentification())
                ->fetch();

            /** @noinspection PhpUndefinedFieldInspection */
            if ($user === null || $user->active === 0) {
                return false;
            }

            $current_user = User::getFromDbObject($user->getData());
            $current_user->setToken($token->getCode());
            $this->setCurrentUser($current_user);

            return $token;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Sets the CurrentUser inside the SlimContainer, so we can access it from everywhere
     * @param User $user
     */
    private function setCurrentUser(User $user)
    {
        $this->container['currentUser'] = $user;
    }

    /**
     * @param Code $apiToken
     * @throws \Interop\Container\Exception\ContainerException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    private function extendApiTokenValidity(Code $apiToken)
    {
        $now = Carbon::now();
        $apiToken->setValidUntil($now->addMinutes(30));

        $code = new CodeModel($this->container);
        $code->update($apiToken);
    }
}