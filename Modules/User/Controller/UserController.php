<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de
 * Date: 19.04.18
 * Time: 12:07
 */

namespace Modules\User\Controller;

use Carbon\Carbon;
use Framework\Classes\AccessDeniedException;
use Framework\Classes\Controller;
use Framework\Classes\MissingPropsException;
use Framework\Mailer;
use Interop\Container\Exception\ContainerException;
use Modules\Code\Classes\Code;
use Modules\Code\Models\CodeModel;
use Modules\MailTemplate\Models\MailTemplateModel;
use Modules\PushNotifications\Models\PushModel;
use Modules\User\Classes\AccountInactiveException;
use Modules\User\Classes\AccountUnknownException;
use Modules\User\Classes\InvalidApiTokenException;
use Modules\User\Classes\MailDeliveryException;
use Modules\User\Classes\MailInUseException;
use Modules\User\Classes\NicknameInUseException;
use Modules\User\Classes\NotSignedInException;
use Modules\User\Classes\UnknownUserTypeException;
use Modules\User\Classes\User;
use Modules\User\Models\UserModel;
use Slim\Container;
use Slim\Http\Request;
use Framework\Classes\AnotherException;
use Slim\Http\UploadedFile;

/**
 * Class UserController
 * @package Modules\User\Controller
 */
class UserController extends Controller
{
    /** @var UserModel */
    private $model;

    /**
     * userController constructor.
     * @param Request $request
     * @param Container $c
     * @throws ContainerException
     */
    public function __construct(Request $request, Container $c)
    {
        parent::__construct($request, $c);

        $this->model = new UserModel($c);
    }

    /**
     * Register a new User (inactive)
     * @PostParam name required
     * @PostParam surname required
     * @PostParam mail required
     * @PostParam nickname required
     * @PostParam password required
     * @PostParam birthday required
     * @param string $name
     * @param string $surname
     * @param string $mail
     * @param string $nickname
     * @param string $password
     * @param string $birthday
     * @return array
     * @throws AnotherException
     * @throws ContainerException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     * @throws \Exception
     */
    public function register(
        string $name,
        string $surname,
        string $mail,
        string $nickname,
        string $password,
        string $birthday
    ): array
    {
        if (strtolower($nickname) === 'gameplay') {
            throw new AnotherException('Nickname kann nicht GamePlay lauten!');
        }

        $user = new User();

        $user
            ->setName($name)
            ->setSurname($surname)
            ->setNickname($nickname)
            ->setMail($mail)
            ->setPassword($password)
            ->setBirthday($birthday);

        try {
            if ($this->model->mailIsInUse($user->getMail())) {
                throw new MailInUseException();
            }

            if ($this->model->nicknameIsInUse($user->getNickname())) {
                throw New NicknameInUseException();
            }

            $user = $this->model->insert($user);

            $code = $this->generateActivationCode($user->getMail(), $user->getPasswordHash());
            $this->sendActivationMail($user, $code);

            return $user->toDbObject();
        } catch (\Exception $exception) {
            $this->model->hardDelete($user);
            throw $exception;
        }
    }

    /**
     * @param User $user
     * @param Code $activation_code
     * @throws \Exception
     * @throws \Interop\Container\Exception\ContainerException
     */
    private function sendActivationMail(User $user, Code $activation_code)
    {
        /** @var Mailer $mailer */
        $mailer = $this->container->get('mailer');
        $mail_template = MailTemplateModel::getUserActivationTemplate($user, $activation_code->getCode());

        $mailer
            ->setSubject('GamePlay Account aktivierung')
            ->setHTML($mail_template)
            ->addReceiver($user->getMail());

        if (!$mailer->send()) {
            throw new MailDeliveryException();
        }
    }

    /**
     * @param string $mail
     * @param string $password_hash
     * @return Code
     * @throws \Exception
     * @throws \Interop\Container\Exception\ContainerException
     */
    private function generateActivationCode(string $mail, string $password_hash): Code
    {
        $activation_code = sha1($mail . $password_hash . random_int(1, 999999999));

        $code = new Code();

        $code->setCode($activation_code);
        $code->setType('activation');
        $code->setIdentification($mail);

        $code_model = new CodeModel($this->container);
        $code = $code_model->insert($code);

        return $code;
    }

    /**
     * Activate a User by Mail & Token
     * @param string $route_nickname
     * @param string $route_token
     * @return array
     * @throws AccountUnknownException
     * @throws ContainerException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     * @throws \Modules\User\Classes\InvalidActivationCodeException
     */
    public function activate(
        string $route_nickname,
        string $route_token
    ): array
    {
        $user = $this->model->activate(
            $route_nickname,
            $route_token
        );

        $this->invalidateActivationCodes($user);

        return $user->toDbObject(
            true,
            false,
            false,
            true,
            false
        );
    }

    /**
     * @param User $user
     * @throws ContainerException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    private function invalidateActivationCodes(User $user)
    {
        $code_model = new CodeModel($this->container);
        $code_model->invalidateCodes('activation', $user->getMail());
    }

    /**
     * Sign a User in
     * @PostParam mailOrNickname required
     * @PostParam password required
     * @param string $mail_or_nickname
     * @param string $password
     * @return array
     * @throws AccountInactiveException
     * @throws AccountUnknownException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function signIn(
        string $mail_or_nickname,
        string $password
    ): array
    {
        $user = $this->model->signIn(
            $mail_or_nickname,
            $password
        );

        $data = $user->toDbObject(true, false, true);
        return $data;
    }

    /**
     * Sign a User out -> delete API-Access-Token
     * @HeadParam token required
     * @return bool
     * @throws NotSignedInException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function signOut(): bool
    {
        if (!$this->request->hasHeader('token')) {
            throw new NotSignedInException();
        }

        $token = $this->request->getHeader('token')[0];
        $this->model->signOut($token);

        return true;
    }

    /**
     * Get a User by its ID
     * @param int $route_id
     * @return User
     * @throws AccessDeniedException
     * @throws AccountUnknownException
     * @throws ContainerException
     * @throws UnknownUserTypeException
     */
    public function getById(
        int $route_id
    ): User
    {
        $result = $this->model->getById(
            $route_id
        );

        /** @var User $current_user */
        $current_user = $this->container->get('currentUser');

        if (
            $current_user->getType() !== User::ADMINISTRATOR &&
            $current_user->getId() !== $route_id
        ) {
            throw new AccessDeniedException();
        }

        return $result;
    }

    /**
     * Get the Current User
     * @return array
     * @throws ContainerException
     */
    public function getCurrent(): array
    {
        /** @var User $current_user */
        $current_user = $this->container->get('currentUser');

        return $current_user->toDbObject(true, false, true);
    }

    /**
     * Get all Users
     * @return array
     * @throws \Modules\User\Classes\UnknownUserTypeException
     */
    public function getAll(): array
    {
        $users = $this->model->getAll();

        $user_objects = [];
        /** @var User $user */
        foreach ($users as $user) {
            $user_objects[] = $user->toDbObject(true, false, false, true, false);
        }

        return $user_objects;
    }

    /**
     * Update a User
     * @PostParam name
     * @PostParam surname
     * @PostParam birthday
     * @param int $route_user_id
     * @param string $name
     * @param string $surname
     * @param string $birthday
     * @return array
     * @throws AccessDeniedException
     * @throws AccountUnknownException
     * @throws ContainerException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function update(
        int $route_user_id,
        string $name,
        string $surname,
        string $birthday
    ): array
    {
        $user = $this->model->getById($route_user_id);

        /** @var User $current_user */
        $current_user = $this->container->get('currentUser');

        if (
            $current_user->getType() !== User::ADMINISTRATOR &&
            $current_user->getId() !== $user->getId()
        ) {
            throw new AccessDeniedException();
        }

        $user->setName($name);
        $user->setSurname($surname);
        $user->setBirthday($birthday);

        return $this->model->update($user)->toDbObject(true, false, false);
    }

    /** @noinspection PhpComposerExtensionStubsInspection */
    /**
     * Upload a new ProfilePicture
     * @param int $route_user_id
     * @return bool
     * @throws AccessDeniedException
     * @throws AccountUnknownException
     * @throws ContainerException
     * @throws MissingPropsException
     * @throws UnknownUserTypeException
     * @throws \ImagickException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function changeProfileImage(
        int $route_user_id
    ): bool
    {
        /** @var User $current_user */
        $current_user = $this->container->get('currentUser');

        if (
            $current_user->getType() !== User::ADMINISTRATOR &&
            $current_user->getId() !== $route_user_id
        ) {
            throw new AccessDeniedException();
        }

        /** @var UploadedFile[] $files */
        $files = $files = $this->request->getUploadedFiles();
        $amount_of_files = \count($files) > 1;

        if ($amount_of_files > 1 || $amount_of_files === 0) {
            throw new MissingPropsException(['Bitte nur eine Datei anhängen']);
        }

        if (!isset($files['file'])) {
            throw new MissingPropsException(['Datei-Anhang fehlt']);
        }

        $file = $files['file'];

        // Limit file-types
        $image_file_type = strtolower(pathinfo($file->getClientFilename(), PATHINFO_EXTENSION));
        if (
            $image_file_type !== 'jpg' &&
            $image_file_type !== 'png' &&
            $image_file_type !== 'jpeg'
            && $image_file_type !== 'gif'
        ) {
            throw new MissingPropsException(['Nur Bilder vom Typ: JPG, JPEG, PNG & GIF sind erlaubt.']);
        }

        // Get user
        $user = $this->model->getById($route_user_id);

        $file_contents = file_get_contents($_FILES['file']['tmp_name']);

        if ($file_contents === false || $file_contents === '') {
            throw new MissingPropsException(['Datei ist zu groß oder fehlerhaft...']);
        }

        $user->setProfileImage($file_contents, $file->getClientMediaType());

        $this->model->update($user);

        return true;
    }

    /**
     * Get The ProfileImage of a User
     * @param int $route_user_id
     * @throws AccountUnknownException
     * @throws AnotherException
     * @throws UnknownUserTypeException
     */
    public function getProfileImage(
        int $route_user_id
    )
    {
        $user = $this->model->getById($route_user_id);

        $img_string = $user->getProfileImage();

        // it is a not a base64 string
        if (strpos($img_string, 'data:image/') !== 0) {
            throw new AnotherException('User hat kein Bild hochgeladen!');
        }

        // remove 'data:' prefix
        $type = substr(
            $img_string,
            strpos($img_string, ':') + 1
        );

        $type = explode(';', $type)[0];

        $img_string = substr($img_string, strpos($img_string, ',') + 1);

        $img_data = base64_decode($img_string);

        header('Content-Type: ' . $type);
        echo $img_data;
        die();
    }

    /**
     * Change the Password of a User with a One-Time-Use Code (received by Mail)
     * @PostParam password required
     * @PostParam code required
     * @param string $route_nickname
     * @param string $password
     * @param string $code
     * @return array
     * @throws AccessDeniedException
     * @throws AccountUnknownException
     * @throws ContainerException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function changePassword(
        string $route_nickname,
        string $password,
        string $code
    ): array
    {
        $code_model = new CodeModel($this->container);
        $db_codes = $code_model->getByTypeAndIdentification('reset_password', $route_nickname);

        $found = false;
        /** @var Code $db_code */
        foreach ($db_codes as $db_code) {
            if ($db_code->getValidUntil() < Carbon::now()) {
                continue;
            }

            if ($db_code->getCode() === $code) {
                $found = true;
                break;
            }
        }

        if (!$found) {
            throw new AccessDeniedException();
        }

        $user = $this->model->getByNickname($route_nickname);

        $user->setPassword($password);
        $user = $this->model->update($user);
        return $user->toDbObject(
            true,
            false,
            false,
            true,
            false
        );
    }

    /**
     * Change a password directly, without sending a One-Time-Code via Mail
     * @PostParam password required
     * @param int $route_user_id
     * @param string $password
     * @return array
     * @throws AccessDeniedException
     * @throws AccountUnknownException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function changePasswordDirectly(
        int $route_user_id,
        string $password
    ): array
    {
        $user = $this->model->getById($route_user_id);

        /** @var User $current_user */
        $current_user = $this->container['currentUser'];
        if ($current_user->getType() !== $user->getType()) {
            throw new AccessDeniedException();
        }

        $user->setPassword($password);
        $user = $this->model->update($user);

        return $user->toDbObject(
            true,
            false,
            false,
            true,
            false
        );
    }

    /**
     * @param string $nickname
     * @return Code
     * @throws ContainerException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     * @throws \Exception
     */
    private function generateResetPasswordCode(string $nickname): Code
    {
        $code_model = new CodeModel($this->container);

        $code = new Code();

        // Valid for 24hours
        $valid_until = new Carbon();
        $valid_until->addDay(1);
        $code->setValidUntil($valid_until);

        $code->setIdentification($nickname);
        $code->setType('reset_password');
        $code->setCode(sha1($nickname . 'reset_password' . random_int(1, 999999999)));

        return $code_model->insert($code);
    }

    /**
     * Change the Nickname of a User
     * @PostParam nickname required
     * @PostParam password required
     * @param int $route_user_id
     * @param string $nickname
     * @param string $password
     * @return bool
     * @throws AccessDeniedException
     * @throws AccountUnknownException
     * @throws AnotherException
     * @throws ContainerException
     * @throws NicknameInUseException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function changeNickname(
        int $route_user_id,
        string $nickname,
        string $password
    ): bool
    {
        if (strtolower($nickname) === 'gameplay') {
            throw new AnotherException('Nickname kann nicht GamePlay lauten!');
        }

        try {
            $this->model->getByNickname($nickname);
            throw new NicknameInUseException();
        } catch (AccountUnknownException $e) {
            // If catched, Nickname is not in Use
        }

        /** @var User $current_user */
        $current_user = $this->container->get('currentUser');
        if (
            $current_user->getType() !== User::ADMINISTRATOR &&
            $current_user->getId() !== $route_user_id
        ) {
            throw new AccessDeniedException();
        }

        $user = $this->model->getById($route_user_id);

        try {
            $this->model->signIn($user->getNickname(), $password, true);
        } catch (\Exception $e) {
            throw new AnotherException('Passwort nicht korrekt');
        }

        if (
            $current_user->getType() !== User::ADMINISTRATOR &&
            $current_user->getId() !== $user->getId()
        ) {
            throw new AccessDeniedException();
        }

        $user->setNickname($nickname);

        $this->model->update($user);

        return true;
    }

    /**
     * Request a One-Time-Code (received by Mail) to change the Mail-Address and verify the new Address exists
     * @PostParam mail required
     * @param int $route_user_id
     * @param string $mail
     * @param string $password
     * @return bool
     * @throws AccessDeniedException
     * @throws AccountUnknownException
     * @throws AnotherException
     * @throws ContainerException
     * @throws MailInUseException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function requestChangeMail(
        int $route_user_id,
        string $mail,
        string $password = ''
    ): bool
    {
        try {
            $this->model->getByMail($mail);
            throw new MailInUseException();
        } catch (AccountUnknownException $e) {
            // If this Error is thrown, Mail is available
        }

        $user = $this->model->getById($route_user_id);

        /** @var User $current_user */
        $current_user = $this->container->get('currentUser');
        if ($current_user->getType() !== User::ADMINISTRATOR) {
            try {
                $this->model->signIn($user->getNickname(), $password, true);
            } catch (\Exception $e) {
                throw new AnotherException('Passwort falsch');
            }
        }

        /** @var User $current_user */
        $current_user = $this->container->get('currentUser');
        if (
            $current_user->getType() !== User::ADMINISTRATOR &&
            $current_user->getId() !== $user->getId()
        ) {
            throw new AccessDeniedException();
        }

        $code = $this->generateChangeMailCode($route_user_id, $mail);
        $this->sendMailChangeRequestMail($current_user, $code);

        return true;
    }

    /**
     * @param int $user_id
     * @param string $password
     * @return Code
     * @throws ContainerException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     * @throws \Exception
     */
    private function generateChangeMailCode(int $user_id, string $password): Code
    {
        $code_model = new CodeModel($this->container);

        $code = new Code();

        $code->setType('change_mail');

        // Valid for 24hours
        $valid_until = new Carbon();
        $valid_until->addDay(1);
        $code->setValidUntil($valid_until);

        $code->setIdentification($user_id);
        $code->setCode(sha1($user_id . 'change-mail' . random_int(1, 999999999)));
        $code->setPayload($password);

        return $code_model->insert($code);
    }

    /**
     * @param User $user
     * @param Code $change_code
     * @throws \Exception
     * @throws \Interop\Container\Exception\ContainerException
     */
    private function sendMailChangeRequestMail(User $user, Code $change_code)
    {
        /** @var Mailer $mailer */
        $mailer = $this->container->get('mailer');
        $mail_template = MailTemplateModel::getMailChangeTemplate($user, $change_code->getCode());

        $mailer
            ->setSubject('GamePlay E-Mail Bestätigung')
            ->setHTML($mail_template)
            ->addReceiver($user->getMail());

        if (!$mailer->send()) {
            throw new MailDeliveryException();
        }
    }

    /**
     * Request a One-Time-Code (received by Mail) to change the current Password
     * @PostParam login required
     * @param string $login
     * @return bool
     * @throws ContainerException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function requestChangePassword(
        string $login
    ): bool
    {
        try {
            $user = $this->model->getByMail($login);
        } catch (AccountUnknownException $e) {
            try {
                $user = $this->model->getByNickname($login);
            } catch (AccountUnknownException $e) {
                return true;
            }
        }

        $code = $this->generateResetPasswordCode($user->getNickname());
        $this->sendPasswordChangeRequestMail($user, $code);

        return true;
    }

    /**
     * @param User $user
     * @param Code $change_code
     * @throws \Exception
     * @throws \Interop\Container\Exception\ContainerException
     */
    private function sendPasswordChangeRequestMail(User $user, Code $change_code)
    {
        /** @var Mailer $mailer */
        $mailer = $this->container->get('mailer');
        $mail_template = MailTemplateModel::getPasswordResetTemplate($user, $change_code->getCode());

        $mailer
            ->setSubject('GamePlay Passwort-Änderung')
            ->setHTML($mail_template)
            ->addReceiver($user->getMail());

        if (!$mailer->send()) {
            throw new MailDeliveryException();
        }
    }

    /**
     * Change the Mail by using a One-Time-Code
     * @PostParam code required
     * @param string $route_nickname
     * @param string $code
     * @return array
     * @throws AccountUnknownException
     * @throws InvalidApiTokenException
     * @throws UnknownUserTypeException
     */
    public function changeMail(
        string $route_nickname,
        string $code
    ): array
    {
        $user = $this->model->getByNickname($route_nickname);

        $user = $this->model->changeMail(
            $user->getId(),
            $code
        );

        $user = $this->model->getById($user->getId());

        return $user->toDbObject();
    }

    /**
     * Change the active State of a User
     * @PostParam nextState required
     * @param int $route_user_id
     * @param bool $next_state
     * @return array
     * @throws AccountUnknownException
     * @throws ContainerException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function changeActive(
        int $route_user_id,
        bool $next_state
    ): array
    {
        $user = $this->model->getById($route_user_id);

        $user->setActive($next_state);

        $user = $this->model->update($user);

        try {
            $push_model = new PushModel($this->container);
            $push_model->sendToUser(
                $user->getId(),
                'Dein Account wurde aktualisiert',
                'Dein Account wurde ' . ($user->isActive() ? 'Aktiviert' : 'Deaktiviert')
            );
        } catch (\Exception $e) {
        }

        return $user->toDbObject(
            true,
            false,
            false,
            true,
            false
        );
    }

    /**
     * Change the User-Role
     * @PostParam type required
     * @param int $route_user_id
     * @param string $type
     * @return array
     * @throws AccountUnknownException
     * @throws ContainerException
     * @throws UnknownUserTypeException
     * @throws \Modules\DatabaseManager\Classes\SQLException
     */
    public function changeType(
        int $route_user_id,
        string $type
    ): array
    {
        if (
            $type !== User::ADMINISTRATOR &&
            $type !== User::EVENTMANAGER &&
            $type !== User::USER
        ) {
            throw new UnknownUserTypeException();
        }

        $user = $this->model->getById($route_user_id);

        $user->setType($type);
        $user = $this->model->update($user);

        try {
            $push_model = new PushModel($this->container);
            $push_model->sendToUser(
                $user->getId(),
                'Dein Account wurde aktualisiert',
                'Du bist nun ' . $user->getType(),
                ''
            );
        } catch (\Exception $e) {
        }

        return $user->toDbObject(
            true,
            false,
            false,
            true,
            false
        );
    }
}