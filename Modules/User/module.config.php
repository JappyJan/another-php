<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 22.04.18
 * Time: 11:26
 */

use Framework\Classes\Route;
use Modules\User\Middleware\AccessControl;

return [
    'routing' => [
        new Route(
            Route::HTTP_PUT,
            '/user',
            \Modules\User\Controller\UserController::class,
            'register'
        ),

        new Route(
            Route::HTTP_GET,
            '/user/{userId}/profile-image',
            \Modules\User\Controller\UserController::class,
            'getProfileImage'
        ),

        new Route(
            Route::HTTP_POST,
            '/user/activate/{nickname}/{token}',
            \Modules\User\Controller\UserController::class,
            'activate'
        ),

        new Route(
            Route::HTTP_POST,
            '/user/sign-in',
            \Modules\User\Controller\UserController::class,
            'signIn'
        ),

        new Route(
            Route::HTTP_POST,
            '/user/sign-out',
            \Modules\User\Controller\UserController::class,
            'signOut',
            [],
            [
                '*'
            ]
        ),

        new Route(
            Route::HTTP_POST,
            '/user/request-change-password',
            \Modules\User\Controller\UserController::class,
            'requestChangePassword'
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{userId}',
            \Modules\User\Controller\UserController::class,
            'update',
            [],
            [
                '*'
            ]
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{userId}/change-image',
            \Modules\User\Controller\UserController::class,
            'changeProfileImage',
            [],
            ['*']
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{nickname}/password-change',
            \Modules\User\Controller\UserController::class,
            'changePassword',
            [],
            []
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{userId}/password-change-direct',
            \Modules\User\Controller\UserController::class,
            'changePasswordDirectly',
            [],
            ['*']
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{userId}/change-nickname',
            \Modules\User\Controller\UserController::class,
            'changeNickname',
            [],
            ['*']
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{userId}/change-active',
            \Modules\User\Controller\UserController::class,
            'changeActive',
            [],
            [\Modules\User\Classes\User::ADMINISTRATOR]
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{userId}/change-type',
            \Modules\User\Controller\UserController::class,
            'changeType',
            [],
            [\Modules\User\Classes\User::ADMINISTRATOR]
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{userId}/request-change-mail',
            \Modules\User\Controller\UserController::class,
            'requestChangeMail',
            [],
            ['*']
        ),

        new Route(
            Route::HTTP_POST,
            '/user/{nickname}/change-mail',
            \Modules\User\Controller\UserController::class,
            'changeMail',
            [],
            []
        ),

        new Route(
            Route::HTTP_GET,
            '/user',
            \Modules\User\Controller\UserController::class,
            'getAll',
            [],
            [
                \Modules\User\Classes\User::ADMINISTRATOR
            ]
        ),

        new Route(
            Route::HTTP_GET,
            '/user/me',
            \Modules\User\Controller\UserController::class,
            'getCurrent',
            [],
            ['*']
        ),

        new Route(
            Route::HTTP_GET,
            '/user/{id}',
            \Modules\User\Controller\UserController::class,
            'getById',
            [],
            [
                \Modules\User\Classes\User::ADMINISTRATOR
            ]
        ),
    ],

    'middleware' => [
        AccessControl::class
    ],

    'databaseTables' => [
        \Modules\User\Classes\User::class
    ]
];