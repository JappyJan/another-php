<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Modules\User\Classes;

use Framework\Classes\AnotherException;

/**
 * Class MailInUseException
 * @package Modules\User\Classes
 */
class MailInUseException extends AnotherException
{
    public function __construct()
    {
        parent::__construct('E-Mail wird bereits verwendet');
    }
}