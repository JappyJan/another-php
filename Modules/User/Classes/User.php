<?php /** @noinspection PhpUndefinedFieldInspection */

/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 18.04.18
 * Time: 14:33
 */

namespace Modules\User\Classes;

use Carbon\Carbon;
use Framework\Classes\Database\Table;
use Framework\Classes\Database\TableColumn;
use Framework\Classes\Database\DbObject;
use Modules\User\Classes\NicknameContainsWhitespaceException;
use Modules\User\Classes\NotLivingException;

/**
 * Class User
 * @package Modules\User\Classes
 */
class User implements DbObject
{
    const USER = 'User';
    const ADMINISTRATOR = 'Administrator';

    /** @var integer */
    private $id = 0;
    /** @var string */
    private $passwordHash;
    /** @var boolean */
    private $active = false;
    /** @var string */
    private $name;
    /** @var string */
    private $surname;
    /** @var string */
    private $nickname;
    /** @var string */
    private $mail;
    /** @var Carbon */
    private $birthday;
    /** @var string */
    private $type = User::USER;
    /** @var string */
    private $token;
    /** @var string */
    private $profileImage = '';

    /**
     * @param array $dbObject
     * @return User
     * @throws UnknownUserTypeException
     */
    public static function getFromDbObject(array $dbObject): User
    {
        $user = new self();
        $user->setId((int)$dbObject['id']);
        $user->setPassword($dbObject['passwordHash'], true);
        $user->setActive((bool)$dbObject['active']);
        $user->setName($dbObject['name']);
        $user->setSurname($dbObject['surname']);
        $user->setNickname($dbObject['nickname']);
        $user->setMail($dbObject['mail']);
        $user->setBirthday(new Carbon($dbObject['birthday']));
        $user->setType($dbObject['type']);
        $user->setProfileImageFromDb($dbObject['profileImage']);

        return $user;
    }

    /**
     * @param bool $withId
     * @param bool $withPasswordHash
     * @param bool $withToken
     * @param bool $withProfileImageBool
     * @param bool $withProfileImageString
     * @return array
     */
    public function toDbObject(
        bool $withId = false,
        bool $withPasswordHash = false,
        bool $withToken = false,
        bool $withProfileImageBool = true,
        bool $withProfileImageString = false
    ): array
    {
        $object = [
            'active' => $this->isActive(),
            'name' => $this->getName(),
            'surname' => $this->getSurname(),
            'nickname' => $this->getNickname(),
            'mail' => $this->getMail(),
            'birthday' => $this->getBirthday()->toDateTimeString(),
            'type' => $this->getType(),
        ];

        if ($withId) {
            $object['id'] = $this->getId();
        }

        if ($withPasswordHash) {
            $object['passwordHash'] = $this->getPasswordHash();
        }

        if ($withToken) {
            $object['api_token'] = $this->getToken();
        }

        if ($withProfileImageBool) {
            $object['gotProfileImage'] = $this->getProfileImage() !== '';
        }

        if ($withProfileImageString) {
            $object['profileImage'] = $this->getProfileImage();
        }

        return $object;
    }

    /** @return \Framework\Classes\Database\Table[]
     * @throws \Exception
     */
    public function getDatabaseTables(): array
    {
        $user_table = Table::new('user')
            ->addColumns(
                [
                    new TableColumn('id', TableColumn::TYPE_INT, 11, false),
                    new TableColumn('created', TableColumn::TYPE_TIMESTAMP, 0, false, 'CURRENT_TIMESTAMP'),
                    new TableColumn('passwordHash', TableColumn::TYPE_VARCHAR, 512, false),
                    new TableColumn('active', TableColumn::TYPE_BOOLEAN, 0, false),
                    new TableColumn('name', TableColumn::TYPE_VARCHAR, 512, false),
                    new TableColumn('surname', TableColumn::TYPE_VARCHAR, 512, false),
                    new TableColumn('nickname', TableColumn::TYPE_VARCHAR, 512, false),
                    new TableColumn('mail', TableColumn::TYPE_VARCHAR, 512, false),
                    new TableColumn('birthday', TableColumn::TYPE_DATE, 0, false),
                    new TableColumn('type', TableColumn::TYPE_VARCHAR, 100, false),
                    new TableColumn('profileImage', TableColumn::TYPE_LONG_TEXT, 0, false),
                    new TableColumn('internetAccess', TableColumn::TYPE_BOOLEAN, 0, false),
                ]
            )
            ->setPrimaryKey('id', true)
            ->addUniqueColumn('nickname', 'nickname')
            ->addUniqueColumn('mail', 'mail');

        return [
            $user_table
        ];
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @param string $password
     * @param bool $alreadyHashed
     * @return User
     */
    public function setPassword(string $password, bool $alreadyHashed = false): User
    {
        if (!$alreadyHashed) {
            $this->passwordHash = password_hash($password, PASSWORD_DEFAULT);
        } else {
            $this->passwordHash = $password;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     *
     * @return User
     */
    public function setNickname(string $nickname): User
    {
        if (!!strpos($nickname, " ")) {
            throw new NicknameContainsWhitespaceException();
        }
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return User
     */
    public function setToken(string $token): User
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return User
     */
    public function setActive(bool $active): User
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return User
     */
    public function setName(string $name): User
    {
        $parts = explode(" ", $name);
        
        $this->name = ucfirst(array_shift($parts));
        foreach($parts as $part) {
            $this->name .= ucfirst($part);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     *
     * @return User
     */
    public function setSurname(string $surname): User
    {
        $parts = explode(" ", $surname);
        
        $this->surname = ucfirst(array_shift($parts));
        foreach($parts as $part) {
            $this->surname .= ucfirst($part);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     *
     * @return User
     */
    public function setMail(string $mail): User
    {
        $this->mail = $mail;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getBirthday(): Carbon
    {
        return $this->birthday;
    }

    /**
     * @param Carbon|string $birthday
     *
     * @return User
     */
    public function setBirthday($birthday): User
    {
        if (\is_string($birthday)) {
            $birthday = new Carbon($birthday);
        }

        /** @var Carbon $birthday */
        if ($birthday->diffInDays(Carbon::now()) < 0) {
            throw new NotLivingException();
        }

        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return User
     * @throws UnknownUserTypeException
     */
    public function setType(string $type): User
    {
        if (
            $type !== self::ADMINISTRATOR &&
            $type !== self::USER
        ) {
            Throw new UnknownUserTypeException();
        }

        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfileImage(): string
    {
        return $this->profileImage;
    }


    /**
     * @param string $profileImage
     * @return User
     */
    public function setProfileImageFromDb(string $profileImage): User
    {
        $this->profileImage = $profileImage;
        return $this;
    }/** @noinspection PhpComposerExtensionStubsInspection */

    /**
     * @param string $imageBlob
     * @param string $imageType
     * @return User
     * @throws \ImagickException
     */
    public function setProfileImage(string $imageBlob, string $imageType): User
    {
        /** @noinspection PhpComposerExtensionStubsInspection */
        $image = new \Imagick();

        $image->readImageBlob($imageBlob);

        $width = $image->getImageWidth();
        $height = $image->getImageHeight();

        $size = $width > $height ? $height : $width;
        $xPos = ($width / 2) - ($size / 2);

        if ($imageType === 'image/gif') {
            $image = $image->coalesceImages();
            foreach ($image as $frame) {
                $frame->cropImage($size, $size, $xPos, 0);
                $frame->scaleImage(500, 500);
            }
            $image = $image->deconstructImages();
            $newImageBlob = $image->getImagesBlob();
        } else {
            $image->cropImage($size, $size, $xPos, 0);
            $image->scaleImage(500, 500);
            $newImageBlob = $image->getImageBlob();
        }

        $imageBase64 = base64_encode($newImageBlob);

        $this->profileImage = 'data:' . $imageType . ';base64,' . $imageBase64;

        return $this;
    }
}