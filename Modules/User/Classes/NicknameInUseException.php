<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Modules\User\Classes;

use Framework\Classes\AnotherException;

/**
 * Class NicknameInUseException
 * @package Modules\User\Classes
 */
class NicknameInUseException extends AnotherException
{
    public function __construct()
    {
        parent::__construct('Nickname wird bereits verwendet');
    }
}