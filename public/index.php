<?php
/**
 * @noinspection PhpUnhandledExceptionInspection
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 22.02.18
 * Time: 18:45
 */
require __DIR__ . '/../Framework/AnotherPHP.php';
AnotherPHP::run();