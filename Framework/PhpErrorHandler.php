<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 28.06.18
 * Time: 09:39
 */

namespace Framework;


use Framework\Classes\AnotherException;
use Framework\Classes\Response\JsonError;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PhpErrorHandler
 * @package Framework
 */
class PhpErrorHandler
{
    /** @var Container */
    private $container;

    /**
     * ErrorHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param \Throwable $error
     * @return Response
     * @throws Classes\ModuleConfigNotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __invoke(Request $request, Response $response, \Throwable $error)
    {
        if ($this->container->has('logger')) {
            /** @var Logger $logger */
            $logger = $this->container->get('logger');

            if ($error instanceof AnotherException) {
                $logger->addError($error->getMessage(), $error->getTrace());
            } else {
                $logger->addCritical($error->getMessage(), $error->getTrace());
            }
        }
        return $response
            ->withStatus(500)
            ->withJson(new JsonError($error));
    }
}