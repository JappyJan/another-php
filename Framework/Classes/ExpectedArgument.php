<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 20:05
 */

namespace Framework\Classes;


/**
 * Class ExpectedArgument
 * @package Framework\Classes
 */
class ExpectedArgument
{
    public $name;
    public $type;

    /**
     * ExpectedArgument constructor.
     * @param string $name
     * @param string $type
     */
    public function __construct(string $name, string $type = null)
    {
        $this->name = $name;
        $this->type = $type;
    }
}