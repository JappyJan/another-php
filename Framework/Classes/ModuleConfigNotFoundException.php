<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Framework\Classes;

/**
 * Class ModuleConfigNotFoundException
 * @package Framework\Classes
 */
class ModuleConfigNotFoundException extends AnotherException
{
    /**
     * ModuleConfigNotFoundException constructor.
     * @param string $moduleName
     */
    public function __construct(string $moduleName)
    {
        parent::__construct("Module '" . $moduleName . "' is missing module.config.php");
    }
}