<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Framework\Classes;

/**
 * Class AnotherException
 * @package Framework\Classes
 */
class AnotherException extends \Exception
{
    private $statusCode;

    /**
     * AnotherException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code = 0)
    {
        $this->statusCode = $code;
        parent::__construct($message);
    }

    /**
     * @return int
     */
    public function getStatusCode(): int {
        return $this->statusCode;
    }
}