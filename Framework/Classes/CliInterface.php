<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 19.04.18
 * Time: 11:14
 */

namespace Framework\Classes;


use Monolog\Logger;
use Slim\Container;


/**
 * Class CliInterface
 * @package Framework\Classes
 */
class CliInterface
{
    /** @var Container */
    protected $container;
    /** @var array */
    protected $args;
    /** @var array */
    protected $config;
    /** @var Logger */
    protected $logger;

    /**
     * AbstractController constructor.
     * @param array $args
     * @param Container $c
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(
        array $args,
        Container $c
    )
    {
        $this->config = $c->get('config');
        $this->logger = $c->get('logger');
        $this->container = $c;
        $this->args = $args;
    }
}