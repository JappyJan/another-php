<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 19.04.18
 * Time: 11:14
 */

namespace Framework\Classes;

use Framework\Mailer;
use LessQL\Database;
use Monolog\Logger;
use Slim\Container;
use Slim\Http\Request;

/**
 * Class AbstractController
 * @package Framework\Classes
 */
class Controller
{
    /** @var Request */
    protected $request;

    /** @var Container */
    protected $container;

    /** @var Database */
    protected $db;

    /** @var array */
    protected $config;

    /** @var Mailer */
    protected $mailer;

    /** @var Logger */
    protected $logger;

    /**
     * AbstractController constructor.
     * @param Request $request
     * @param Container $c
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(
        Request $request,
        Container $c
    )
    {
        $this->db = $c->get('db');
        $this->config = $c->get('config');
        $this->mailer = $c->get('mailer');
        $this->logger = $c->get('logger');
        $this->request = $request;
        $this->container = $c;
    }
}