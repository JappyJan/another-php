<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 18.04.18
 * Time: 14:41
 */

namespace Framework\Classes;

/**
 * Class Route
 * @package Framework\Classes
 */
class Route
{
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';
    const HTTP_PUT = 'PUT';
    const HTTP_DELETE = 'DELETE';
    const HTTP_OPTIONS = 'OPTIONS';

    /** @var String */
    public $method;
    /** @var String */
    public $route;
    /** @var AbstractModel */
    public $class;
    /** @var String */
    public $function;
    /** @var array */
    public $middleware;
    /** @var array */
    public $allowedTypes;

    /**
     * Route constructor.
     * @param string $method
     * @param string $route
     * @param $class
     * @param string $function
     * @param array $middleware
     * @param array $allowedTypes
     */
    public function __construct(string $method, string $route, $class, string $function, array $middleware = [], array $allowedTypes = [])
    {
        $this->method = $method;
        $this->route = $route;
        $this->class = $class;
        $this->function = $function;
        $this->middleware = $middleware;
        $this->allowedTypes = $allowedTypes;
    }
}