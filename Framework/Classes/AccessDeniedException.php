<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Framework\Classes;

/**
 * Class MailDeliveryException
 * @package Framework\Classes
 */
class AccessDeniedException extends AnotherException
{
    public function __construct()
    {
        parent::__construct('Zugang verweigert!');
    }
}