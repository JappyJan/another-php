<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 18.04.18
 * Time: 15:02
 */

namespace Framework\Classes;


use LessQL\Database;
use Monolog\Logger;
use Slim\Container;

/**
 * Class AbstractModel
 * @package Framework\Classes
 */
class AbstractModel
{
    /** @var Database */
    protected $db;
    /** @var array */
    protected $config;
    /** @var Logger */
    protected $logger;
    /** @var Container */
    protected $container;

    /**
     * AbstractModel constructor.
     * @param Container $c
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(
        Container $c
    )
    {
        $this->db = $c->get('db');
        $this->config = $c->get('config');
        $this->logger = $c->get('logger');
        $this->container = $c;
    }
}