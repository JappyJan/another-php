<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 25.04.18
 * Time: 22:33
 */

namespace Framework\Classes\Middleware;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Interface MiddleWareInterface
 * @package Framework\Classes\Middleware
 */
interface Middleware
{
    /**
     * MiddleWareInterface constructor.
     * @param Container $c
     */
    public function __construct(Container $c);

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, $next);
}