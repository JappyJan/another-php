<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 20.04.18
 * Time: 23:47
 */

namespace Framework\Classes\Response;


/**
 * Class jsonResponse
 * @package Framework\Classes\Response
 */
class JsonResponse
{
    public $code = JsonResponse::CODE_SUCCESS;
    public $message = '';
    public $data;

    const CODE_SUCCESS = 0;

    //descriptions
    const CODE_MISSING_PROPS = 1;
    const CODE_ACCESS_DENIED = 2;
    const CODE_USER_NOT_SIGNED_IN = 3;
    const CODE_ERROR = 4;


    /**
     * JsonResponse constructor.
     * @param int $code
     * @param $data
     */
    public function __construct(int $code, $data)
    {
        if ($code !== self::CODE_SUCCESS) {
            $this->message = $data;
        } else {
            $this->data = $data;
        }

        $this->code = $code;
    }
}