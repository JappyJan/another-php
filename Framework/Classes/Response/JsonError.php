<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 20.04.18
 * Time: 23:48
 */

namespace Framework\Classes\Response;


use Framework\Classes\AccessDeniedException;
use Framework\Config;

/**
 * Class jsonError
 * @package Framework\Classes\Response
 */
class JsonError
{
    public $code = JsonResponse::CODE_ERROR;
    public $trace;
    public $message;

    /**
     * JsonError constructor.
     * @param \Exception | \Throwable $e
     * @throws \Framework\Classes\ModuleConfigNotFoundException
     */
    public function __construct ($e)
    {
        if (
            !$e instanceof AccessDeniedException &&
            Config::get()['settings']['displayErrorDetails'] === true
        ){
            $this->message = $e->getMessage();
            $this->message .= ' (';
            $this->message .= $e->getFile() . ' :: ' . $e->getLine();
            $this->message .= ')';
            $this->trace = $e->getTrace();
        } else {
            $this->message = 'Ein Fehler ist aufgetreten!';
        }
    }
}