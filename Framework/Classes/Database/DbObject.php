<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 19.04.18
 * Time: 10:09
 */

namespace Framework\Classes\Database;

/**
 * Interface DbObject
 * @package Framework\Classes\Database
 */
interface DbObject
{
    /** @noinspection ReturnTypeCanBeDeclaredInspection */
    /**
     * @param array $dbObject
     *
     * @return self
     */
    public static function getFromDbObject(array $dbObject);

    /**
     * @param bool $withId
     * @return array
     */
    public function toDbObject(bool $withId = false): array;

    /** @return Table[] */
    public function getDatabaseTables(): array;
}