<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 27.04.18
 * Time: 17:33
 */

namespace Framework\Classes\Database;


/**
 * Class DatabaseTableColumn
 * @package Framework\Classes\Database
 */
class TableColumn
{
    const TYPE_BOOLEAN = 'TINYINT';
    const TYPE_INT = 'INT';
    const TYPE_TINYINT = 'TINYINT';
    const TYPE_SMALLINT = 'SMALLINT';
    const TYPE_MEDIUMINT = 'MEDIUMINT';
    const TYPE_BIGINT = 'BIGINT';
    const TYPE_VARCHAR = 'VARCHAR';
    const TYPE_TEXT = 'TEXT';
    const TYPE_LONG_TEXT = 'LONGTEXT';
    const TYPE_DATE = 'DATE';
    const TYPE_DATETIME = 'DATETIME';
    const TYPE_TIMESTAMP = 'TIMESTAMP';
    const TYPE_FLOAT = 'FLOAT';

    /** @var string */
    private $name;
    /** @var string */
    private $type;
    /** @var int */
    private $length;
    /** @var bool */
    private $nullable;
    /** @var string */
    private $default;

    /**
     * DatabaseTableColumn constructor.
     *
     * @param string $name
     * @param string $type
     * @param int $length
     * @param bool $nullable
     * @param string $default
     *
     * @throws \Exception
     */
    public function __construct(
        string $name,
        string $type,
        int $length,
        bool $nullable = true,
        string $default = ''
    )
    {
        $this->name = $name;
        $this->type = $type;

        if (
            $type === self::TYPE_DATE ||
            $type === self::TYPE_DATETIME ||
            $type === self::TYPE_TIMESTAMP ||
            $type === self::TYPE_FLOAT
        ) {
            $length = 0;
        } else if ($type === self::TYPE_BOOLEAN) {
            $length = 1;
        } else if (
            $type !== self::TYPE_TEXT &&
            $type !== self::TYPE_LONG_TEXT &&
            $length === 0
        ) {
            Throw New \RuntimeException('Length might not be 0 for type ' . $type);
        }

        $this->length = $length;
        $this->nullable = $nullable;
        $this->default = $default;
    }

    /**
     * @param $serializedColumn
     * @return TableColumn
     * @throws \Exception
     */
    public
    static function deserialize($serializedColumn): TableColumn
    {
        return new TableColumn(
            $serializedColumn['name'],
            $serializedColumn['type'],
            $serializedColumn['length'],
            $serializedColumn['nullable'],
            $serializedColumn['default']
        );
    }

    /**
     * @return array
     */
    public
    function serialize(): array
    {
        return [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'length' => $this->getLength(),
            'nullable' => $this->isNullable(),
            'default' => $this->getDefault()
        ];
    }

    /**
     * @return string
     */
    public
    function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TableColumn
     */
    public
    function setName(string $name): TableColumn
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public
    function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return TableColumn
     */
    public
    function setType(string $type): TableColumn
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public
    function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     *
     * @return TableColumn
     */
    public
    function setLength(int $length): TableColumn
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return bool
     */
    public
    function isNullable(): bool
    {
        return $this->nullable;
    }

    /**
     * @param bool $nullable
     *
     * @return TableColumn
     */
    public
    function setNullable(bool $nullable): TableColumn
    {
        $this->nullable = $nullable;
        return $this;
    }

    /**
     * @return string
     */
    public
    function getDefault(): string
    {
        return $this->default;
    }

    /**
     * @param string $default
     *
     * @return TableColumn
     */
    public
    function setDefault(string $default): TableColumn
    {
        $this->default = $default;
        return $this;
    }
}