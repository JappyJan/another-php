<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 27.04.18
 * Time: 17:27
 */

namespace Framework\Classes\Database;


/**
 * Class DatabaseTable
 *
 * @package Framework\Classes\Database
 */
class Table
{
    /** @var string */
    private $name;
    /** @var TableColumn[] */
    private $columns = [];
    /** @var string */
    private $primaryKeyName = '';
    /** @var bool */
    private $primaryKeyAutoIncrement = false;
    /** @var array */
    private $uniqueColumns = [];

    /**
     * DatabaseTable constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     * @return Table
     */
    public static function new(string $name): Table
    {
        return new Table($name);
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        $data = [
            'name' => $this->getName(),
            'primaryKey' => [
                'name' => $this->getPrimaryKeyName(),
                'isAutoIncrement' => $this->primaryKeyIsAutoIncrement()
            ],
            'columns' => [],
            'uniqueColumns' => $this->getUniqueColumns()
        ];

        foreach ($this->getColumns() as $column) {
            $data['columns'][] = $column->serialize();
        }

        return $data;
    }

    /**
     * @param array $data
     * @return Table
     * @throws \Exception
     */
    public static function deserialize(array $data): Table
    {
        $table = self::new($data['name']);

        foreach ($data['columns'] as $serialized_column) {
            $table->addColumn(TableColumn::deserialize($serialized_column));
        }

        $table->setPrimaryKey($data['primaryKey']['name'], $data['primaryKey']['isAutoIncrement']);

        foreach ($data['uniqueColumns'] as $unique_column) {
            $table->addUniqueColumn($unique_column['name'], $unique_column['key']);
        }

        return $table;
    }

    /**
     * @param array $columns
     *
     * @return $this
     */
    public function addColumns(Array $columns): self
    {
        foreach ($columns as $column) {
            $this->addColumn($column);
        }
        return $this;
    }

    /**
     * @param \Framework\Classes\Database\TableColumn $column
     *
     * @return $this
     */
    public function addColumn(TableColumn $column): self
    {
        $this->columns[] = $column;
        return $this;
    }

    /**
     * @param string $column_name
     *
     * @return $this
     */
    public function removeColumn(string $column_name): self
    {
        $this->columns = array_filter(
            $this->columns,
            Function (TableColumn $column) use ($column_name) {
                return !($column->getName() === $column_name);
            }
        );
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Table
     */
    public function setName(string $name): Table
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return TableColumn[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param TableColumn[] $columns
     *
     * @return Table
     */
    public function setColumns(array $columns): Table
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * @param string $columnName
     * @param bool $isAutoIncrement
     *
     * @return $this
     */
    public function setPrimaryKey(string $columnName, bool $isAutoIncrement = false): self
    {
        $this->primaryKeyAutoIncrement = $isAutoIncrement;
        $this->primaryKeyName = $columnName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryKeyName(): string
    {
        return $this->primaryKeyName;
    }

    /**
     * @return bool
     */
    public function primaryKeyIsAutoIncrement(): bool
    {
        return $this->primaryKeyAutoIncrement;
    }

    /**
     * @return array
     */
    public function getUniqueColumns(): array
    {
        return $this->uniqueColumns;
    }

    /**
     * @param string $keyName
     * @param string $columnName
     * @return Table
     */
    public function addUniqueColumn(string $keyName, string $columnName): Table
    {
        $this->uniqueColumns[] = ['keyName' => $keyName, 'columnName' => $columnName];
        return $this;
    }
}