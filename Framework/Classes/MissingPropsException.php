<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 21.05.18
 * Time: 22:38
 */

namespace Framework\Classes;

/**
 * Class MissingPropsException
 * @package Framework\Classes
 */
class MissingPropsException extends AnotherException
{
    private $errors;
    /**
     * ModuleConfigNotFoundException constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        parent::__construct('Nicht alle benötigten Argumente wurden angegeben!');
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array  {
        return $this->errors;
    }
}