<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 16.04.18
 * Time: 00:06
 */

namespace Framework;

use Monolog\Logger;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Class Mailer
 *
 * @package Framework
 */
class Mailer
{
    /**
     * @var array
     */
    private $config;
    /** @var */
    private $mailer;
    /** @var \Monolog\Logger */
    private $logger;

    /**
     * Mailer constructor.
     *
     * @param array $config
     *
     * @param \Monolog\Logger $logger
     *
     * @throws \Exception
     */
    public function __construct(array $config, Logger $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
        $this->mailer = new PHPMailer();

        $this->configure();
    }

    /**
     * @throws \Exception
     */
    private function configure()
    {
        Try {
            $this->mailer->isHTML(true);
            $this->mailer->SMTPDebug = $this->config['SMTPDebug'];
            if ($this->config['isSMTP']) {
                $this->mailer->isSMTP();
            }
            $this->mailer->SMTPAuth = $this->config['SMTPAuth'];
            $this->mailer->Port = $this->config['Port'];
            $this->mailer->Host = $this->config['Host'];
            $this->mailer->Username = $this->config['Username'];
            $this->mailer->Password = $this->config['Password'];
            $this->mailer->SMTPSecure = $this->config['SMTPSecure'];
            $this->mailer->SMTPOptions = $this->config['SMTPOptions'];

            $this->mailer->setFrom($this->config['From']['E-Mail'], $this->config['From']['Alias']);

            $this->mailer->addReplyTo($this->config['ReplyTo']['E-Mail'], $this->config['ReplyTo']['Alias']);
        } catch (\Exception $e) {
            $this->logger->addError('Mailer Configuration Failed!', $e);
            throw $e;
        }
    }

    /**
     * @param string $email
     * @param string $alias
     *
     * @return \Framework\Mailer
     */
    public function addReceiver(string $email, string $alias = ''): Mailer
    {
        if ($alias !== '') {
            $this->mailer->addAddress($email, $alias);
        } else {
            $this->mailer->addAddress($email);
        }

        return $this;
    }

    /**
     * @param array $receivers
     *
     * @return \Framework\Mailer
     */
    public function addReceivers(array $receivers): Mailer
    {
        foreach ($receivers as $receiver) {
            $alias = '';

            if (\is_array($receiver)) {
                $email = $receiver['email'];
                $alias = $receiver['alias'];
            } else {
                $email = $receiver;
            }

            $this->addReceiver($email, $alias);
        }

        return $this;
    }

    /**
     * @param string $email
     * @param string $alias
     *
     * @return \Framework\Mailer
     */
    public function addBCC(string $email, string $alias): Mailer
    {
        $this->mailer->addBCC($email, $alias);

        return $this;
    }

    /**
     * @param array $bccS
     *
     * @return $this
     */
    public function addBCCs(array $bccS): self
    {
        foreach ($bccS as $bcc) {
            $alias = '';
            if (\is_array($bcc)) {
                $email = $bcc['email'];
                $alias = $bcc['alias'];
            } else {
                $email = $bcc;
            }

            $this->addBCC($email, $alias);
        }

        return $this;
    }

    /**
     * @param string $email
     * @param string $alias
     *
     * @return \Framework\Mailer
     */
    public function addCC(string $email, string $alias): Mailer
    {
        $this->mailer->addCC($email, $alias);

        return $this;
    }

    /**
     * @param array $CCs
     *
     * @return $this
     */
    public function addCCs(array $CCs): self
    {
        foreach ($CCs as $cc) {
            $alias = '';
            if (\is_array($cc)) {
                $email = $cc['email'];
                $alias = $cc['alias'];
            } else {
                $email = $cc;
            }

            $this->addCC($email, $alias);
        }

        return $this;
    }

    /**
     * @param string $filePath
     * @param string $fileName
     *
     * @return \Framework\Mailer
     * @throws \Exception
     */
    public function addAttachment(string $filePath, string $fileName): Mailer
    {
        try {
            $this->mailer->addAttachment($filePath, $fileName);
        } catch (\Exception $e) {
            $this->logger->addError('Mailer could not Attach File!', $e);
            throw $e;
        }
        return $this;
    }

    /**
     * @param string $subject
     *
     * @return \Framework\Mailer
     */
    public function setSubject(string $subject): Mailer
    {
        $this->mailer->Subject = $subject;

        return $this;
    }

    /**
     * @param string $html
     *
     * @return \Framework\Mailer
     */
    public function setHTML(string $html): Mailer
    {
        $this->mailer->Body = $html;

        return $this;
    }

    /**
     * @param string $text
     *
     * @return \Framework\Mailer
     */
    public function setText(string $text): Mailer
    {
        $this->mailer->AltBody = $text;

        return $this;
    }

    /**
     * @return bool
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function send(): bool
    {
        if ($this->mailer->send()) {
            return true;
        }

        throw new Exception($this->mailer->ErrorInfo);
    }

    /**
     * @return PHPMailer
     */
    public function getMailer(): PHPMailer
    {
        return $this->mailer;
    }
}