<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 28.06.18
 * Time: 09:39
 */

namespace Framework;


use Framework\Classes\AnotherException;
use Framework\Classes\Response\JsonError;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ErrorHandler
 * @package Framework
 */
class ErrorHandler
{
    /** @var Container */
    private $container;

    /**
     * ErrorHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param \Exception $exception
     * @return Response
     * @throws Classes\ModuleConfigNotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __invoke(Request $request, Response $response, \Exception $exception)
    {
        if ($this->container->has('logger')) {
            /** @var Logger $logger */
            $logger = $this->container->get('logger');

            if ($exception instanceof AnotherException) {
                $logger->addError($exception->getMessage(), $exception->getTrace());
            } else {
                $logger->addCritical($exception->getMessage(), $exception->getTrace());
            }
        }

        return $response
            ->withStatus(500)
            ->withJson(new JsonError($exception));
    }
}