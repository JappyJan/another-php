<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 28.06.18
 * Time: 09:39
 */

namespace Framework;


use Framework\Classes\Response\JsonResponse;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class NotFoundHandler
 * @package Framework
 */
class NotFoundHandler
{
    /** @var Container */
    private $container;

    /**
     * ErrorHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __invoke(Request $request, Response $response)
    {
        if ($this->container->has('logger')) {
            /** @var Logger $logger */
            $logger = $this->container->get('logger');

            $logger->addError('Unknown URL', [
                'route' => $request->getUri(),
                'method' => $request->getMethod(),
                'params' => $request->getParsedBody(),
                'queryParams' => $request->getQueryParams(),
            ]);
        }

        return $response
            ->withStatus(500)
            ->withJson(new JsonResponse(JsonResponse::CODE_ERROR, 'UNKNOWN URL'));
    }
}