<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de
 * Date: 22.02.18
 * Time: 19:49
 */

use Framework\Classes\AnotherException;
use Framework\Classes\MissingPropsException;
use Framework\Classes\Response\JsonError;
use Framework\Classes\Response\JsonResponse;
use Framework\ErrorHandler;
use Framework\NotAllowedHandler;
use Framework\NotFoundHandler;
use Framework\PhpErrorHandler;
use Slim\Http\Request;
use Slim\Http\Response;

require __DIR__ . '/../vendor/autoload.php';

/**
 * Class AnotherPHP
 */
class AnotherPHP
{
    /**
     * @var \Slim\App
     */
    private $app;

    /**
     * Create a new Application Instance, and Run it
     * @throws Exception
     */
    public static function run()
    {
        self::registerAutoloader();

        global $argv;

        if (PHP_SAPI === 'cli') {
            self::setCliEnvironment($argv);
        }

        $app = new self();

        $app->createApp();

        $app->buildDic();

        if (PHP_SAPI === 'cli') {
            $app->fireCliCommand($argv);
        } else {
            $app->registerMiddleware();

            $app->registerRoutes();
        }
        $app->launch();
    }

    /**
     * @param $argv
     */
    private static function setCliEnvironment($argv)
    {
        if ($argv === null) {
            return;
        }

        if (!$argv[0] === '-stage') {
            return;
        }

        array_shift($argv);

        ini_set('stage', array_shift($argv));
    }

    /**
     *Register the Class Autoloader
     */
    private static function registerAutoloader()
    {
        require 'SplClassLoader.php';
        $autoloader = new SplClassLoader();
        $autoloader->setIncludePath(__DIR__ . '/../');
        $autoloader->register();
    }

    /**
     * Create The Application
     * @throws Exception
     */
    private function createApp()
    {
        $config = \Framework\Config::get();
        $this->app = new \Slim\App($config);

        ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);

        if ($config['settings']['displayErrorDetails']) {
            ini_set('display_startup_errors', 1);
        ini_set('display_errors', 1);
            error_reporting(E_ALL);
        }
    }

    /**
     * Build the Slim Container
     */
    private function buildDic()
    {
        $container = $this->app->getContainer();

        $container['errorHandler'] = function () use ($container) {
            return new ErrorHandler($container);
        };

        $container['notFoundHandler'] = function () use ($container) {
            return new NotFoundHandler($container);
        };

        $container['notAllowedHandler'] = function () use ($container) {
            return new NotAllowedHandler($container);
        };

        $container['phpErrorHandler'] = function () use ($container) {
            return new PhpErrorHandler($container);
        };

        $container['logger'] = function () {
            $logger = new \Monolog\Logger('Framework');
            $log_file_path = \Framework\Config::get('logger')['logFile'];
            $file_handler = new \Monolog\Handler\StreamHandler($log_file_path);
            $logger->pushHandler($file_handler);
            return $logger;
        };

        $container['config'] = function ($c) {
            return $c;
        };

        $container['db'] = function () {
            $db = \Framework\Config::get('db');
            /** @noinspection SpellCheckingInspection */
            $pdo = new PDO(
                'mysql:host=' . $db['host'] . ';dbname=' . $db['db'],
                $db['user'],
                $db['pass']
            );
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            $db = new \LessQL\Database($pdo);

            //$this->databaseSetRequiredFields($db, $db['requiredFields']);

            /*
             $db->setQueryCallback(function($query, $params) {
                echo $query;
                print_r($params);
            });
            */

            return $db;
        };

        $container['mailer'] = function (\Slim\Container $c) {
            return new \Framework\Mailer($c->get('config')['mail'], $this->app->getContainer()->get('logger'));
        };
    }

    /**
     * Register Middleware's Defined by Modules
     */
    private function registerMiddleware()
    {
        /** @var \Framework\Classes\Middleware\Middleware $middleware */
        foreach ($this->app->getContainer()->get('config')['middleware'] as $middleware) {
            /** @var \Slim\Container $c */
            $c = $this->app->getContainer();
            $this->app->add(new $middleware($c));
        }
    }

    /**
     * Register all Routes defined in Modules
     * @throws Exception
     */
    private function registerRoutes()
    {
        /**
         * @var $route Framework\Classes\Route
         */
        foreach ($this->app->getContainer()->get('config')['routing'] as $route) {
            /**
             * @param Request $request
             * @param Response $response
             * @param array $queryArgs
             * @return Response
             */
            $run_function = Function (Request $request, Response $response, array $queryArgs) use ($route) {
                try {
                    $controller = new $route->class($request, $this);

                    if (!method_exists($controller, $route->function)) {
                        throw new RuntimeException('Controller Function not declared!');
                    }

                    $reflectionMethod = new ReflectionMethod($controller, $route->function);
                    $requiredPost = [];
                    $requiredRoute = [];
                    $arguments = [];

                    foreach ($reflectionMethod->getParameters() as $parameter) {
                        $name = $parameter->getName();
                        $isRouteParam = strpos($name, 'route_') === 0;

                        if ($isRouteParam) {
                            $argumentValue = $queryArgs[str_replace('route_', '', lcfirst($parameter->getName()))];
                        } else {
                            try {
                                $argumentValue = $request->getParsedBody()[$parameter->getName()];
                            } catch(Exception $e) {
                                $argumentValue = null;
                            }
                        }

                        $arguments[] = $argumentValue;

                        if ($parameter->isOptional()) {
                            continue;
                        }

                        $argument = new \Framework\Classes\ExpectedArgument(
                            $parameter->getName(),
                            $parameter->getType()
                        );

                        if ($isRouteParam) {
                            $requiredRoute[] = $argument;
                            continue;
                        }

                        $requiredPost[] = $argument;
                    }

                    if (\count($requiredRoute) > 0) {
                        AnotherPHP::validateRequestArgs($requiredRoute, $queryArgs);
                    }

                    if (\count($requiredPost) > 0) {
                        AnotherPHP::validateRequestArgs($requiredPost, $request->getParsedBody());
                    }

                    return $response->withJson(
                        new JsonResponse(
                            JsonResponse::CODE_SUCCESS,
                            call_user_func_array([$controller, $route->function], $arguments)
                        )
                    );
                } catch (Exception $exception) {
                    if ($exception instanceof MissingPropsException) {
                        return $response->withJson(new JsonResponse(JsonResponse::CODE_MISSING_PROPS, $exception->getErrors()));
                    }

                    if ($exception instanceof AnotherException) {
                        $response = $response
                            ->withJson(
                                new JsonResponse(
                                    JsonResponse::CODE_ERROR,
                                    $exception->getMessage()
                                )
                            );

                        if ($exception->getStatusCode() !== 0) {
                            $response = $response->withStatus($exception->getStatusCode());
                        }

                        return $response;
                    }

                    return $response->withJson(new JsonError($exception));
                }
            };

            switch ($route->method) {
                case 'GET':
                    $route_call = $this->app->get($route->route, $run_function);
                    break;

                case 'POST':
                    $route_call = $this->app->post($route->route, $run_function);
                    break;

                case 'PUT':
                    $route_call = $this->app->put($route->route, $run_function);
                    break;

                case 'DELETE':
                    $route_call = $this->app->delete($route->route, $run_function);
                    break;

                case 'OPTIONS':
                    $route_call = $this->app->options($route->route, $run_function);
                    break;
                default:
                    throw new RuntimeException('HTTP Method of RouteConfig unknown!');
            }

            // Register Middleware
            if ($route->middleware !== null && $route->middleware !== null) {
                $c = $this->app->getContainer();
                /** @var \Framework\Classes\Middleware\Middleware $middleware */
                foreach ($route->middleware as $middleware) {
                    $midWare = new $middleware($c);
                    $route_call->add($midWare);
                }
            }
        }
    }

    /**
     * Start The Application
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    private function launch()
    {
        $this->app->run();
    }

    /**
     * @param array $expects
     * @param array $args
     * @return void
     * @throws MissingPropsException
     * @throws Exception
     */
    public static function validateRequestArgs(array $expects, $args)
    {
        $errors = [];
        foreach ($expects as $expect) {
            $error = self::validateRequestArg($expect, $args);
            if ($error !== '') {
                $errors[] = $error;
            }
        }

        if (\count($errors) > 0) {
            throw new MissingPropsException($errors);
        }
    }

    /**
     * @param \Framework\Classes\ExpectedArgument $expected
     * @param $args
     * @return string
     * @throws Exception
     */
    private static function validateRequestArg(\Framework\Classes\ExpectedArgument $expected, $args): string
    {
        $error = '';
        $name = $expected->name;
        $isRouteParam = strpos($name, 'route_') === 0;
        $name = lcfirst(
            str_replace(
                'route_',
                "" ,
                $name
            )
        );
        if (!isset($args[$name])) {
            if ($isRouteParam) {
                $error .= 'Route-Param ';
            }
            $error .= $name . "(" . $expected->type . ")' muss angegeben werden!";
        } else if (
            $expected->type !== null &&
            !self::checkType($expected->type, $args[$name])
        ) {
            if ($isRouteParam) {
                $error .= 'Route-Param ';
            }
            $error .= $name . ' muss vom Typ ' . $expected->type . ' sein!';
        }

        return $error;
    }

    /**
     * @param string $type
     * @param $value
     * @return bool
     * @throws Exception
     */
    private static function checkType(string $type, $value): bool
    {
        if ($type === 'string') {
            return true;
        }

        switch ($type) {
            case 'int':
                $filter = FILTER_VALIDATE_INT;
                break;
            case 'integer':
                $filter = FILTER_VALIDATE_INT;
                break;
            case 'float':
                $filter = FILTER_VALIDATE_FLOAT;
                break;
            case 'bool':
                $filter = FILTER_VALIDATE_BOOLEAN;
                break;
            case 'boolean':
                $filter = FILTER_VALIDATE_BOOLEAN;
                break;
            default:
                throw new Exception('Unknown Argument-Type');
        }

        $result = filter_var('' . $value, $filter, FILTER_NULL_ON_FAILURE) !== null;
        return $result;
    }

    /**
     * @param $argv
     * @throws \Framework\Classes\ModuleConfigNotFoundException
     */
    public function fireCliCommand($argv)
    {
        if ($argv === null) {
            die("Kein Befehl angegeben!\r\n");
        }

        array_shift($argv); //remove filename
        $command = array_shift($argv); //remove command

        $conf = \Framework\Config::get('cli');

        if (!isset($conf[$command])) {
            echo "Unbekannter Befehl!\r\n";
            die();
        }

        $conf = $conf[$command];
        $controller = new $conf['class']($_SERVER['argv'], $this->app->getContainer());

        if (!method_exists($controller, $conf['function'])) {
            throw new RuntimeException('Controller Function not declared!');
        }

        $controller->{$conf['function']}();
        die();
    }
}
