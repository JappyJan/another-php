<?php
/**
 * Created by JanJaap Web-Solutions
 *
 * Jan Jaap
 *  https://janjaap.de
 *  mail@janjaap.de

 * Date: 22.02.18
 * Time: 19:16
 */

namespace Framework;
use Framework\Classes\ModuleConfigNotFoundException;

/**
 * Class Config
 * @package Framework
 */
class Config
{
    /**
     * @param string|null $section
     *
     * @return array | string
     * @throws ModuleConfigNotFoundException
     */
	public static function get (string $section = null)
    {
        $stage = getenv('stage');

	    $config = require __DIR__ . '/../configs/default.config.php';
	
	    if (file_exists(__DIR__ . '/../configs/' . $stage . '.config.php')) {
            /** @noinspection PhpIncludeInspection */
		    $stage_config = require __DIR__ . '/../configs/' . $stage . '.config.php';

		    $config = self::replaceArrayProps($config, $stage_config);
        }

        if (!isset($config['middleware'])) {
            $config['middleware'] = [];
        }

        if (!isset($config['routing'])) {
            $config['routing'] = [];
        }

        $modules = require __DIR__ . '/../configs/modules.php';
        foreach ($modules as $module) {
            if (file_exists(__DIR__ . '/../Modules/' . $module . '/module.config.php')) {
                /** @noinspection PhpIncludeInspection */
                $module_config = require __DIR__ . '/../Modules/' . $module . '/module.config.php';

                /** @noinspection SlowArrayOperationsInLoopInspection */
                $config = array_merge_recursive($config, $module_config);
            } else {
                Throw new ModuleConfigNotFoundException($module);
            }
        }
	
	    if ($section !== null) {
		    return $config[$section];
	    }
        return $config;
    }

    /**
     * @param array $original
     * @param array $replacement
     * @return array
     */
    private static function replaceArrayProps(array $original, array $replacement): array {
	    foreach ($replacement as $key => $value) {
	        if (\is_array($value) && isset($original[$key]) && \is_array($original[$key])) {
	            $original[$key] = self::replaceArrayProps($original[$key], $value);
            } else {
	            $original[$key] = $value;
            }
        }

        return $original;
    }
}